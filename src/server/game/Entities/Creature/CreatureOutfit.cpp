#include "CreatureOutfit.h"
#include "Player.h"

EquipmentSlots  const itemSlots[] =
{
	EQUIPMENT_SLOT_HEAD,
	EQUIPMENT_SLOT_SHOULDERS,
	EQUIPMENT_SLOT_BODY,
	EQUIPMENT_SLOT_CHEST,
	EQUIPMENT_SLOT_WAIST,
	EQUIPMENT_SLOT_LEGS,
	EQUIPMENT_SLOT_FEET,
	EQUIPMENT_SLOT_WRISTS,
	EQUIPMENT_SLOT_HANDS,
	EQUIPMENT_SLOT_BACK,
	EQUIPMENT_SLOT_TABARD,
	EQUIPMENT_SLOT_END
};
Outfit OutfitManager::ReadOutfit(uint64 guid)
{
	//PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_CREATURE_OUTFIT);
	//stmt->setInt64(0, guid);
	//PreparedQueryResult result = WorldDatabase.Query(stmt);

	//if (!result)
	//{

	//}

	//Field* fields = result->Fetch();

	//Outfit _outfit;
	//_outfit._displayId = fields[0].GetInt32();
	//_outfit._gender = fields[1].GetInt8();
	//_outfit._race = fields[2].GetInt8();
	//_outfit._class = fields[3].GetInt8();

	////Player Bytes
	//_outfit._playerBytes._skin = fields[4].GetInt8();
	//_outfit._playerBytes._face = fields[5].GetInt8();
	//_outfit._playerBytes._hair = fields[6].GetInt8();
	//_outfit._playerBytes._haircolor = fields[7].GetInt8();
	//_outfit._playerBytes._facialhair = fields[8].GetInt8();

	////Itemset
	//for (EquipmentSlots const* itr = &itemSlots[0]; *itr != EQUIPMENT_SLOT_END; ++itr)
	//{
	//	//Item set
	//	_outfit.itemEntry.push_back(fields[8 + int(itr)].GetInt32());
	//}

	//return _outfit;
	return Outfit();
}

void OutfitManager::LoadAllOutfits()
{

	QueryResult result = CharacterDatabase.PQuery("SELECT * FROM creature_outfit");
	if (!result)
		return;

	do
	{
		Field* fields = result->Fetch();
		Outfit _outfit;

		_outfit._guid = fields[0].GetInt32();
		_outfit._displayId = fields[1].GetInt32();
		_outfit._gender = fields[2].GetInt8();
		_outfit._race = fields[3].GetInt8();
		_outfit._class = fields[4].GetInt8();

		//Player Bytes
		_outfit._playerBytes._skin = fields[5].GetInt8();
		_outfit._playerBytes._face = fields[6].GetInt8();
		_outfit._playerBytes._hair = fields[7].GetInt8();
		_outfit._playerBytes._haircolor = fields[8].GetInt8();
		_outfit._playerBytes._facialhair = fields[9].GetInt8();

		//Itemset
		for (int i = 10; i < 21; i++)
		{
			//Item set
			//_outfit.itemEntry.insert(i, fields[i].GetInt32());
		}

		Creature* unit = ObjectAccessor::GetObjectInWorld(_outfit._guid, (Creature*)NULL);
		if (unit)
		{
			outfits.push_back(_outfit);
			//unit.outfit = _outfit;
		}
		else
			TC_LOG_DEBUG("creature_outfit", "Could not find creature with guid %u", _outfit._guid);

	} while (result->NextRow());

}

void OutfitManager::DeleteOutfit(uint64 guid)
{
	
}