
#ifndef _CREATURE_OUTFIT
#define _CREATURE_OUTFIT

#include "Field.h"
#include "Define.h"

#include "SharedDefines.h" // Gender
#include <memory>


struct PlayerBytes
{
public:

	uint8 _skin;
	uint8 _face;
	uint8 _hair;
	uint8 _haircolor;
	uint8 _facialhair;
	
};

class Outfit
{
public:
	Outfit() : _displayId(0), _gender(0), _race(0), _class(0), _guild(0), _guid(0)
	{

	}
	//generic
	uint32 _displayId;
	uint8 _gender;
	uint8 _race;
	uint8 _class;

	//player speciffic bytes
	PlayerBytes _playerBytes;

	//Equipment
	std::vector<uint32> itemEntry;

	uint64 _guild;

	uint64 _guid;
};

class OutfitManager
{
public:

	Outfit ReadOutfit(uint64 guid);
	void LoadAllOutfits();
	void DeleteOutfit(uint64 guid);


	std::vector<Outfit> outfits;
};


#endif