//Syndicate-WoW | Cataclysm

#include "ArenaTeamMgr.h"
#include "BattlegroundMgr.h"
#include "Chat.h"
#include "DisableMgr.h"
#include "Language.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "ScriptMgr.h"
#include "Formulas.h"
#include "InfoMgr.h"

//Modified and Simplified by -Most Infamous

class SaveNewCap : public PlayerScript
{
public:
    SaveNewCap() : PlayerScript("SaveNewCap") { }

    void OnLogout(Player* player)
    {
        if (player->GetCurrency(CURRENCY_TYPE_CONQUEST_POINTS, true))
        {
            PreparedStatement* stmt = NULL;
            uint32 capArena = Trinity::Currency::ConquestRatingCalculator(player->GetMaxPersonalArenaRatingRequirement(0)) * CURRENCY_PRECISION;
            uint32 capRBG = Trinity::Currency::BgConquestRatingCalculator(player->GetRbgOrSoloQueueRatingForCapCalculation()) * CURRENCY_PRECISION;
            uint32 cap = std::max(capArena, capRBG);
            stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_PLAYER_CURRENCY_NEW_CAP_OVERALL);
            stmt->setUInt32(0, cap);
            stmt->setUInt64(1, player->GetGUIDLow());
            CharacterDatabase.Execute(stmt);

            if (player->GetCurrency(CURRENCY_TYPE_CONQUEST_META_ARENA, true))
            {
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_PLAYER_CURRENCY_NEW_CAP_ARENA);
                stmt->setUInt32(0, capArena);
                stmt->setUInt64(1, player->GetGUIDLow());
                CharacterDatabase.Execute(stmt);
            }

            if (player->GetCurrency(CURRENCY_TYPE_CONQUEST_META_RBG, true))
            {
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_PLAYER_CURRENCY_NEW_CAP_RBG);
                stmt->setUInt32(0, capRBG);
                stmt->setUInt64(1, player->GetGUIDLow());
                CharacterDatabase.Execute(stmt);
            }
        }
    }

};

class arena_spectator_commands : public CommandScript
{
public:
    arena_spectator_commands() : CommandScript("arena_spectator_commands") { }

    static bool HandleSpectateCommand(ChatHandler* handler, const char *args)
    {
        Player* target;
        uint64 target_guid;
        std::string target_name;
        if (!handler->extractPlayerTarget((char*)args, &target, &target_guid, &target_name))
            return false;

        Player* player = handler->GetSession()->GetPlayer();
        if (target == player || target_guid == player->GetGUID())
        {
            handler->SendSysMessage("You can't teleport to yourself!");
            handler->SetSentErrorMessage(true);
            player->CLOSE_GOSSIP_MENU();
            return false;
        }

        if (player->isInCombat())
        {
            handler->SendSysMessage("Can't do that while in combat!");
            handler->SetSentErrorMessage(true);
            player->CLOSE_GOSSIP_MENU();
            return false;
        }

        if (!target)
        {
            handler->SendSysMessage("The selected player does not exist or is offline!");
            handler->SetSentErrorMessage(true);
            player->CLOSE_GOSSIP_MENU();
            return false;
        }

        if (player->GetPet())
        {
            handler->PSendSysMessage("You must dismiss your pet!");
            handler->SetSentErrorMessage(true);
            player->CLOSE_GOSSIP_MENU();
            return false;
        }

        if (player->GetMap()->IsBattlegroundOrArena() && !player->isSpectator())
        {
            handler->PSendSysMessage("You are already in a battleground or arena!");
            handler->SetSentErrorMessage(true);
            player->CLOSE_GOSSIP_MENU();
            return false;
        }

        Map* cMap = target->GetMap();
        if (!cMap->IsBattleArena())
        {
            handler->PSendSysMessage("No player found in arena!");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (Battleground* bg = target->GetBattleground())
            if ((bg->GetStatus() != STATUS_IN_PROGRESS) && !player->isGameMaster())
            {
                handler->PSendSysMessage("Battleground not started yet!");
                handler->SetSentErrorMessage(true);
                return false;
            }

        if (player->GetMap()->IsBattleground())
        {
            handler->PSendSysMessage("Can't do that while you are in battleground!");
            handler->SetSentErrorMessage(true);
            return false;
        }

        // all's well, set bg id
        // when porting out from the bg, it will be reset to 0
        player->SetBattlegroundId(target->GetBattlegroundId(), target->GetBattlegroundTypeId());
        // remember current position as entry point for return at bg end teleportation
        if (!player->GetMap()->IsBattlegroundOrArena())
            player->SetBattlegroundEntryPoint();

        if (target->isSpectator())
        {
            handler->PSendSysMessage("Can`t do that. Your target is a spectator!");
            handler->SetSentErrorMessage(true);
            return false;
        }

        // stop flight if need
        if (player->isInFlight())
        {
            player->GetMotionMaster()->MovementExpired();
            player->CleanupAfterTaxiFlight();
        }
        // save only in non-flight case
        else
            player->SaveRecallPosition();

        // search for two teams
        Battleground* bGround = target->GetBattleground();
        if (!bGround)
            return false;
        if (bGround->isRated())
        {
            uint32 slot = bGround->GetArenaType() - 2;
            if (bGround->GetArenaType() > 3)
                slot = 2;
            uint32 firstTeamID = target->GetArenaTeamId(slot);
            uint32 secondTeamID = 0;
            Player *firstTeamMember = target;
            Player *secondTeamMember = NULL;
            for (Battleground::BattlegroundPlayerMap::const_iterator itr = bGround->GetPlayers().begin(); itr != bGround->GetPlayers().end(); ++itr)
                if (Player* tmpPlayer = ObjectAccessor::FindPlayer(itr->first))
                {
                    if (tmpPlayer->isSpectator())
                        continue;

                    uint32 tmpID = tmpPlayer->GetArenaTeamId(slot);
                    if (tmpID != firstTeamID && tmpID > 0)
                    {
                        secondTeamID = tmpID;
                        secondTeamMember = tmpPlayer;
                        break;
                    }
                }

                if (firstTeamID > 0 && secondTeamID > 0 && secondTeamMember)
                {
                    ArenaTeam *firstTeam = sArenaTeamMgr->GetArenaTeamById(firstTeamID);
                    ArenaTeam *secondTeam = sArenaTeamMgr->GetArenaTeamById(secondTeamID);
                    if (firstTeam && secondTeam)
                    {
                        handler->PSendSysMessage("|CFFDF01D7Teams: %s vs %s", firstTeam->GetName().c_str(), secondTeam->GetName().c_str());
                        handler->PSendSysMessage("|CFFDF01D7Rating: %u(%u) - %u(%u)", firstTeam->GetRating(), firstTeam->GetAverageMMR(firstTeamMember->GetGroup()), secondTeam->GetRating(), secondTeam->GetAverageMMR(secondTeamMember->GetGroup()));
                        handler->PSendSysMessage("|CFFDF01D7Tip: .spec leave to leave arena.");
                        handler->PSendSysMessage("|CFFDF01D7Tip: .spec watch to spectate from the viewpoint of the targeted player.");
						handler->PSendSysMessage("|CFFDF01D7Tip: .spec reset to leave viewpoint.");
                    }
                }
        }

        // to point to see at target with same orientation
        float x, y, z;
        target->GetContactPoint(player, x, y, z);

        player->TeleportTo(target->GetMapId(), x, y, z, player->GetAngle(target), TELE_TO_GM_MODE);
        player->SetPhaseMask(target->GetPhaseMask(), true);
        player->SetSpectate(true);

        return true;
    }

    static bool HandleSpectateCancelCommand(ChatHandler* handler, const char* /*args*/)
    {
        Player* player = handler->GetSession()->GetPlayer();

        if (!player->isSpectator())
        {
            handler->PSendSysMessage("You are not a spectator!");
            handler->SetSentErrorMessage(true);
            return false;
        }

        player->TeleportToBGEntryPoint();

        return true;
    }

    static bool HandleSpectateFromCommand(ChatHandler* handler, const char *args)
    {
        Player* target;
        uint64 target_guid;
        std::string target_name;
        if (!handler->extractPlayerTarget((char*)args, &target, &target_guid, &target_name))
            return false;

        Player* player = handler->GetSession()->GetPlayer();

        if (!target)
        {
            handler->PSendSysMessage("Can't find player!");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (!player->isSpectator())
        {
            handler->PSendSysMessage("You are not a spectator, spectate someone first!");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (target->isSpectator() && target != player)
        {
            handler->PSendSysMessage("Can`t do that. Your target is a spectator!");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player->GetMap() != target->GetMap())
        {
            handler->PSendSysMessage("Can't do that. Different arenas!?");
            handler->SetSentErrorMessage(true);
            return false;
        }

        // check for arena preperation
        // if exists than battle didn`t begin
        if (target->HasAura(32728) || target->HasAura(32727))
        {
            handler->PSendSysMessage("Can't do that. Arena didn`t start yet!");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player->GetViewpoint())
            player->StopCastingBindSight();
        else
            player->CastSpell(target, 6277, true);
			ChatHandler(player->GetSession()).SendSysMessage("You can leave by clicking ESC button.");

        return true;
    }

    static bool HandleSpectateResetCommand(ChatHandler* handler, const char *args)
    {
        Player* player = handler->GetSession()->GetPlayer();

        if (!player)
        {
            handler->PSendSysMessage("Can't find player!");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (!player->isSpectator() || !player->InArena())
        {
            handler->PSendSysMessage("You are not a spectator!");
            handler->SetSentErrorMessage(true);
            return false;
        }

        Map::PlayerList const &PlList = player->GetMap()->GetPlayers();
        if (PlList.isEmpty())
            return true;
        for (Map::PlayerList::const_iterator i = PlList.begin(); i != PlList.end(); ++i)
        {
            if (Player* pPlayer = i->getSource())
            {
                if (pPlayer->isGameMaster() || pPlayer->isSpectator())
                    continue;

                pPlayer->m_arenaSpectatorFlags = ASPEC_ALL_FLAGS;
            }
        }

        return true;
    }

    static bool HandleForceBGCommand(ChatHandler* handler, const char *args)
    {
        if (!*args)
            return false;

        uint32 bgtypeid = atoi(args);
        sWorld->SetForcedBG(BattlegroundTypeId(bgtypeid));

        return true;
    }

    std::vector<ChatCommand> GetCommands() const override
    {
        static std::vector<ChatCommand> spectateCommandTable =
        {
			//{ "spectate", SEC_GAMEMASTER, false, &HandleSpectateCommand, "" },
            { "watch",    SEC_PLAYER, false, &HandleSpectateFromCommand,  "" },
            //{ "reset",    SEC_PLAYER, false, &HandleSpectateFromCommand, "" },
            { "leave",    SEC_PLAYER, false, &HandleSpectateCancelCommand,"" },
            //{ "setbg",    SEC_GAMEMASTER, false, &HandleForceBGCommand,       "" },
        };

        static std::vector<ChatCommand> commandTable =
        {
            { "spectator", SEC_GAMEMASTER, false, NULL, "", spectateCommandTable },
        };
        return commandTable;
    }
};

enum NpcSpectatorActions
{
    // will be used for scrolling
    NPC_SPECTATOR_ACTION_CANCEL = 500,
    NPC_SPECTATOR_ACTION_LIST_GAMES_2v2 = 2000,
    NPC_SPECTATOR_ACTION_LIST_GAMES_3v3 = 4000,
    NPC_SPECTATOR_ACTION_PLAYER = 6000,
};

const uint16 TopGamesRating = 2000;
const uint8 GamesOnPage = 20;

class npc_arena_spectator : public CreatureScript
{
public:
    npc_arena_spectator() : CreatureScript("npc_arena_spectator") { }

    bool OnGossipHello(Player* pPlayer, Creature* pCreature)
    {
		pPlayer->ADD_GOSSIP_ITEM(9, "|TInterface\\icons\\Achievement_pvp_p_02:40|t Spectate 2v2", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_LIST_GAMES_2v2);
		pPlayer->ADD_GOSSIP_ITEM(9, "|TInterface\\icons\\Achievement_pvp_p_03:40|t Spectate 3v3 Games", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_LIST_GAMES_3v3);
		pPlayer->ADD_GOSSIP_ITEM_EXTENDED(9, "Spectate Game of a Specific Player", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_PLAYER, "", 0, true);
		pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Goodbye!", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_CANCEL);
		pPlayer->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, pCreature->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
    {
        player->PlayerTalkClass->ClearMenus();

		if (action >= NPC_SPECTATOR_ACTION_LIST_GAMES_2v2)
        {
			ShowPage(player, action - NPC_SPECTATOR_ACTION_LIST_GAMES_2v2, SLOT_2VS2);
            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        }
		if (action >= NPC_SPECTATOR_ACTION_LIST_GAMES_3v3)
        {
			ShowPage(player, action - NPC_SPECTATOR_ACTION_LIST_GAMES_3v3, SLOT_3VS3);
            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        }
        if (action == NPC_SPECTATOR_ACTION_CANCEL)
            player->CLOSE_GOSSIP_MENU();
        else
        {
            uint32 teamid = action - NPC_SPECTATOR_ACTION_PLAYER;
            if (ArenaTeam* team = sArenaTeamMgr->GetArenaTeamById(teamid))
            {
                if (Player* target = team->GetFirstMemberInArena())
                {
                    ChatHandler handler(player->GetSession());
                    arena_spectator_commands::HandleSpectateCommand(&handler, target->GetName().c_str());
                }
            }
        }
        return true;
    }

    bool OnGossipSelectCode(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction, const char* code)
    {
        player->PlayerTalkClass->ClearMenus();
        if (uiSender == GOSSIP_SENDER_MAIN)
        {
            switch (uiAction)
            {
            case NPC_SPECTATOR_ACTION_PLAYER:

                std::string name(code);
                if (!name.empty())
                    name[0] = toupper(name[0]);

                ChatHandler handler(player->GetSession());
                arena_spectator_commands::HandleSpectateCommand(&handler, name.c_str());
                return true;
            }
        }

        return false;
    }

    std::string GetClassAndTalentnameById(uint32 id)
    {
        std::string sClass = "";
        switch (id)
        {
        case TALENT_TREE_WARRIOR_ARMS: sClass = " Arms Warrior "; break;
        case TALENT_TREE_WARRIOR_FURY: sClass = " Fury Warrior "; break;
        case TALENT_TREE_WARRIOR_PROTECTION: sClass = " Prot Warrior "; break;
        case TALENT_TREE_PALADIN_HOLY: sClass = " Holy Paladin "; break;
        case TALENT_TREE_PALADIN_PROTECTION: sClass = " Prot Paladin "; break;
        case TALENT_TREE_PALADIN_RETRIBUTION: sClass = " Ret Paladin "; break;
        case TALENT_TREE_HUNTER_BEAST_MASTERY: sClass = " BM Hunter "; break;
        case TALENT_TREE_HUNTER_MARKSMANSHIP: sClass = " MM Hunter "; break;
        case TALENT_TREE_HUNTER_SURVIVAL: sClass = " Survival Hunter "; break;
        case TALENT_TREE_ROGUE_ASSASSINATION: sClass = " Assasin Rogue "; break;
        case TALENT_TREE_ROGUE_COMBAT: sClass = " Combat Rogue "; break;
        case TALENT_TREE_ROGUE_SUBTLETY: sClass = " Sub Rogue "; break;
        case TALENT_TREE_PRIEST_DISCIPLINE: sClass = " Disc Priest "; break;
        case TALENT_TREE_PRIEST_HOLY: sClass = " Holy Priest "; break;
        case TALENT_TREE_PRIEST_SHADOW: sClass = " Shadow Priest "; break;
        case TALENT_TREE_DEATH_KNIGHT_BLOOD: sClass = " Blood DK "; break;
        case TALENT_TREE_DEATH_KNIGHT_FROST: sClass = " Frost DK "; break;
        case TALENT_TREE_DEATH_KNIGHT_UNHOLY: sClass = " Unholy DK "; break;
        case TALENT_TREE_SHAMAN_ELEMENTAL: sClass = " Ele Shaman "; break;
        case TALENT_TREE_SHAMAN_ENHANCEMENT: sClass = " Enh Shaman "; break;
        case TALENT_TREE_SHAMAN_RESTORATION: sClass = " Resto Shaman "; break;
        case TALENT_TREE_MAGE_ARCANE: sClass = " Arcane Mage "; break;
        case TALENT_TREE_MAGE_FIRE: sClass = " Fire Mage "; break;
        case TALENT_TREE_MAGE_FROST: sClass = " Frost Mage "; break;
        case TALENT_TREE_WARLOCK_AFFLICTION: sClass = " Affli Warlock "; break;
        case TALENT_TREE_WARLOCK_DEMONOLOGY: sClass = " Demo Warlock "; break;
        case TALENT_TREE_WARLOCK_DESTRUCTION: sClass = " Destro Warlock "; break;
        case TALENT_TREE_DRUID_BALANCE: sClass = " Boomkin "; break;
        case TALENT_TREE_DRUID_FERAL_COMBAT: sClass = " Feral Druid "; break;
        case TALENT_TREE_DRUID_RESTORATION: sClass = " Resto Druid "; break;
        }
        return sClass;
    }

    std::string GetGamesStringData(Player* player, uint16 mmr)
    {
        std::string teamsMember[BG_TEAMS_COUNT];
        uint32 firstTeamId = 0;
        if (Battleground* arena = player->GetBattleground())
        {
            for (Battleground::BattlegroundPlayerMap::const_iterator itr = arena->GetPlayers().begin(); itr != arena->GetPlayers().end(); ++itr)
            {
                if (Player* member = ObjectAccessor::FindPlayer(itr->first))
                {
                    if (member->isSpectator())
                        continue;

                    uint32 team = itr->second.Team;
                    if (!firstTeamId)
                        firstTeamId = team;

                    teamsMember[firstTeamId == team] += GetClassAndTalentnameById(member->GetPrimaryTalentTree(member->GetActiveSpec()));
                }
            }

            std::stringstream ss;
            ss << teamsMember[0] << " - " << mmr << " - " << teamsMember[1];
            std::string data = ss.str();

            //TC_LOG_INFO("server.loading", "%s", data.c_str());
            return data;
        }
        return "An error occured";
    }

    void ShowPage(Player* player, uint16 page, SlotStoreType slot)
    {
        uint16 lowGames = 0;
        bool haveNextPage = false;
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();

        std::map<uint32, InfoBGEntry> bginfomap = sInfoMgr->GetBgStore(slot);
        for (InfoMgr::BGInfoMap::iterator iter = bginfomap.begin(); iter != bginfomap.end(); ++iter)
        {
            ArenaTeam* team1 = sArenaTeamMgr->GetArenaTeamById(iter->second.team1);
            ArenaTeam* team2 = sArenaTeamMgr->GetArenaTeamById(iter->second.team2);

            if (!team1 || !team2)
                continue;

            Player* member = team1->GetFirstMemberInArena();

            if (!member)
                continue;

			uint16 highestMMR = std::max(iter->second.MMR1, iter->second.MMR2);

            switch (slot)
            {
                case SLOT_2VS2:
                case SLOT_3VS3:
                {
                    lowGames++;
                    if (lowGames)
                        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, GetGamesStringData(member, highestMMR), GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_PLAYER + iter->second.team1);
                    break;
                }
                default:
                    continue;
            }
        }

        if (!lowGames)
        {
            player->PlayerTalkClass->ClearMenus();
            player->CLOSE_GOSSIP_MENU();
        }

        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Nevermind", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_CANCEL);

        if (page > 0)
        {
            switch (slot)
            {
            case SLOT_2VS2:
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "<-Prev", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_LIST_GAMES_2v2 + page - 1);
                break;
            case SLOT_3VS3:
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "<-Prev", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_LIST_GAMES_3v3 + page - 1);
                break;
            default:
                break;
            }
        }

        if (haveNextPage)
        {
            switch (slot)
            {
            case SLOT_2VS2:
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "->Next", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_LIST_GAMES_2v2 + page + 1);
                break;
            case SLOT_3VS3:
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "->Next", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_LIST_GAMES_3v3 + page + 1);
                break;
            default:
                break;
            }
        }
    }
};

void AddSC_Arena_Spectator_npc()
{
    new SaveNewCap();
    new arena_spectator_commands();
    new npc_arena_spectator();
}