//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"
 
// Set USE_TOKEN to 1 if you want to have it use tokens in place of gold
#define USE_TOKEN       0
#define TOKEN_ID        29434
 
struct BloodMoneyInfo
{
        uint64 guid;
        uint32 amount;
        bool accepted;
};
 
typedef std::list<BloodMoneyInfo> BloodMoneyList;
typedef std::map<uint64, BloodMoneyList> BloodMoney;
static BloodMoney m_bloodMoney;
 
bool HasBloodMoneyChallenger(uint64 playerGUID)
{
        return m_bloodMoney.find(playerGUID) != m_bloodMoney.end();
}
 
bool HasBloodMoneyChallenger(uint64 targetGUID, uint64 playerGUID)
{
        if (!HasBloodMoneyChallenger(targetGUID))
                return false;
        BloodMoneyList bml = m_bloodMoney[targetGUID];
        for (BloodMoneyList::const_iterator itr = bml.begin(); itr != bml.end(); ++itr)
                if (itr->guid == playerGUID)
                        return true;
        return false;
}
 
void AddBloodMoneyEntry(uint64 targetGUID, uint64 playerGUID, uint32 amount)
{
        BloodMoneyInfo bmi;
        bmi.guid = playerGUID;
        bmi.amount = amount;
        bmi.accepted = false;
        m_bloodMoney[targetGUID].push_back(bmi);
}
 
void RemoveBloodMoneyEntry(uint64 targetGUID, uint64 playerGUID)
{
        if (!HasBloodMoneyChallenger(targetGUID, playerGUID))
                return;
        BloodMoneyList &list = m_bloodMoney[targetGUID];
        BloodMoneyList::iterator itr;
        for (itr = list.begin(); itr != list.end(); ++itr)
                if (itr->guid == playerGUID)
                        break;
        list.erase(itr);
}
 
void SetChallengeAccepted(uint64 targetGUID, uint64 playerGUID)
{
        if (!HasBloodMoneyChallenger(targetGUID, playerGUID))
                return;
        BloodMoneyList &list = m_bloodMoney[targetGUID];
        BloodMoneyList::iterator itr;
        for (itr = list.begin(); itr != list.end(); ++itr)
        {
                if (itr->guid == playerGUID)
                {
                        itr->accepted = true;
                        break;
                }
        }
}
 
class npc_blood_money : public CreatureScript
{
public :
        npc_blood_money() : CreatureScript("npc_blood_money") {}

bool OnGossipHello(Player * player, Creature * creature)
{
        player->PlayerTalkClass->ClearMenus();
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "|TInterface\\icons\\Ability_dualwield:40|t Challenge Someone", 11, 1000);   
        if (HasBloodMoneyChallenger(player->GetGUID()))
        {
                BloodMoneyList list = m_bloodMoney[player->GetGUID()];
                for (BloodMoneyList::const_iterator itr = list.begin(); itr != list.end(); ++itr)
                {
                        char msg[50];

                        if (Player* plr = Player::GetPlayer(*player, itr->guid))
                        {
                          if (USE_TOKEN)
                          {
                           sprintf(msg, "Accept %s's Challenge of %u tokens", plr->GetName().c_str(), itr->amount);
                           player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, msg, GOSSIP_SENDER_MAIN, itr->guid);
                           sprintf(msg, "Decline %s's Challenge of %u tokens", plr->GetName().c_str(), itr->amount);
                           player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, msg, GOSSIP_SENDER_INFO, itr->guid);
                          }
                          else
                          {
                           sprintf(msg, "Accept %s's Challenge of %ug", plr->GetName().c_str(), itr->amount/10000);
                           player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, msg, GOSSIP_SENDER_MAIN, itr->guid);
                           sprintf(msg, "Decline %s's Challenge of %ug", plr->GetName().c_str(), itr->amount/10000);
                           player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, msg, GOSSIP_SENDER_INFO, itr->guid);
                          }
                               
                        }
                }
        }
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Nevermind", GOSSIP_SENDER_MAIN, 1);
       
        player->SEND_GOSSIP_MENU(80025, creature->GetGUID());

        return true;
}

bool OnGossipSelect(Player * player, Creature * creature, uint32 uiSender, uint32 uiAction)
{
        player->PlayerTalkClass->ClearMenus();
        if (uiAction == 1)
        {
                player->CLOSE_GOSSIP_MENU();
                return true;
        }
        switch(uiSender)
        {
                case GOSSIP_SENDER_MAIN:
                if (Player* target = Player::GetPlayer(*player, uiAction))
                {
                 SetChallengeAccepted(player->GetGUID(), target->GetGUID());
                 char msg[60];
                 sprintf(msg, "%s has accepted your challenge!", player->GetName().c_str());
                 creature->MonsterWhisper(msg, target->GetGUID(), true);
                 player->CLOSE_GOSSIP_MENU();
                }
                break;
                case GOSSIP_SENDER_INFO:
                if (Player* target = Player::GetPlayer(*player, uiAction))
                {
                 char msg[60];
                 sprintf(msg, "%s has declined your challenge!", player->GetName().c_str());
                 creature->MonsterWhisper(msg, target->GetGUID(), true);
                 RemoveBloodMoneyEntry(player->GetGUID(), uiAction);
                 OnGossipHello(player, creature);
                }
                break;
                case 11:
                 if (USE_TOKEN)
                 {
                  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "Bet 5 tokens", GOSSIP_SENDER_MAIN, 5, "", 0, true);
				  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "Bet 10 tokens", GOSSIP_SENDER_MAIN, 10, "", 0, true);
				  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "Bet 15 tokens", GOSSIP_SENDER_MAIN, 15, "", 0, true);
				  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "Bet 25 tokens", GOSSIP_SENDER_MAIN, 25, "", 0, true);
				  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "Bet 50 tokens", GOSSIP_SENDER_MAIN, 50, "", 0, true);
				  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "Bet 100 tokens", GOSSIP_SENDER_MAIN, 100, "", 0, true);
				  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "Bet 150 tokens", GOSSIP_SENDER_MAIN, 150, "", 0, true);
				  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "Bet 200 tokens", GOSSIP_SENDER_MAIN, 200, "", 0, true);
				  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "Bet 250 tokens", GOSSIP_SENDER_MAIN, 250, "", 0, true);
				  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "Bet 500 tokens", GOSSIP_SENDER_MAIN, 500, "", 0, true);
                 }
                 else
                 {
                  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_racial_timeismoney:30|tBet 100 gold", GOSSIP_SENDER_MAIN, 100, "", 0, true);
                  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_racial_timeismoney:30|tBet 500 gold", GOSSIP_SENDER_MAIN, 500, "", 0, true);
                  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_racial_timeismoney:30|tBet 1000 gold", GOSSIP_SENDER_MAIN, 1000, "", 0, true);
                  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_racial_timeismoney:30|tBet 3000 gold", GOSSIP_SENDER_MAIN, 3000, "", 0, true);
                  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_racial_timeismoney:30|tBet 5000 gold", GOSSIP_SENDER_MAIN, 5000, "", 0, true);
                  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_racial_timeismoney:30|tBet 10000 gold", GOSSIP_SENDER_MAIN, 10000, "", 0, true);
                  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_racial_timeismoney:30|tBet 25000 gold", GOSSIP_SENDER_MAIN, 25000, "", 0, true);
                  player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_racial_timeismoney:30|tBet 50000 gold", GOSSIP_SENDER_MAIN, 50000, "", 0, true);
                 }
                
                 player->SEND_GOSSIP_MENU(80025, creature->GetGUID());
                 break;
        }
        return true;
}
 
bool OnGossipSelectCode(Player* player, Creature* creature, uint32 sender, uint32 action, const char* code)
{
        if (player->GetName().c_str() == code)
        {
                ChatHandler(player->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFNow why would you want to challenge yourself?");
                return false;
        }
        if (uint64 targetGUID = sObjectMgr->GetPlayerGUIDByName(code))
        {
 if (Player* target = Player::GetPlayer(*player, targetGUID))
 {
if (target->GetGUID() == player->GetGUID())
{
        ChatHandler(player->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFNow why would you want to challenge yourself?");
        return false;
}
if (target->GetZoneId() == player->GetZoneId())
{
if (USE_TOKEN)
{
 if (target->GetItemCount(TOKEN_ID) < action)
 {
         ChatHandler(player->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFThat player does not have enough tokens to make the bet!");
         player->CLOSE_GOSSIP_MENU();
         return false;
 }
 if (player->GetItemCount(TOKEN_ID) < action)
 {
         ChatHandler(player->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYou do not have enough tokens to make the bet!");
         player->CLOSE_GOSSIP_MENU();
         return false;
 }
 
 bool found = false;
 if (HasBloodMoneyChallenger(player->GetGUID()))
 {
         BloodMoneyList list = m_bloodMoney[player->GetGUID()];
         for (BloodMoneyList::const_iterator itr = list.begin(); itr != list.end(); ++itr)
                 if (itr->guid == target->GetGUID())
                         found = true;
 }
 if (!found)
 {
         if (!HasBloodMoneyChallenger(target->GetGUID(), player->GetGUID()))
         {
                 AddBloodMoneyEntry(target->GetGUID(), player->GetGUID(), action);
                 char msg[60];
                 sprintf(msg, "%s has requested a Duel Challenge duel with you!", player->GetName().c_str());
                 creature->MonsterWhisper(msg, target->GetGUID(), true);
         }
         else
                 ChatHandler(target->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYou cannot request a duel with the same person!");
 }
 else
         ChatHandler(player->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYou cannot request a duel with somebody that has challenged you!");
 player->CLOSE_GOSSIP_MENU();
 return true;
}
else
{
 uint32 money = action*10000;
 if (target->GetMoney() < money)
 {
         ChatHandler(player->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFThat player does not have enough gold to make the bet!");
         player->CLOSE_GOSSIP_MENU();
         return false;
 }
 if (player->GetMoney() < money)
 {
         ChatHandler(player->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYou do not have enough gold to make the bet!");
         player->CLOSE_GOSSIP_MENU();
         return false;
 }

 bool found = false;
 if (HasBloodMoneyChallenger(player->GetGUID()))
 {
         BloodMoneyList list = m_bloodMoney[player->GetGUID()];
         for (BloodMoneyList::const_iterator itr = list.begin(); itr != list.end(); ++itr)
                 if (itr->guid == target->GetGUID())
                         found = true;
 }
 if (!found)
 {
         if (!HasBloodMoneyChallenger(target->GetGUID(), player->GetGUID()))
         {
                 AddBloodMoneyEntry(target->GetGUID(), player->GetGUID(), money);
                 char msg[60];
                 sprintf(msg, "%s has requested a Duel Challenge with you!", player->GetName().c_str());
                 creature->MonsterWhisper(msg, target->GetGUID(), true);
         }
         else
                 ChatHandler(target->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYou cannot request a duel with the same person!");
 }
 else
         ChatHandler(player->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYou cannot request a duel with somebody that has challenged you!");
 player->CLOSE_GOSSIP_MENU();
 return true;
}
       
}
else
{
        ChatHandler(player->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFThat player is not in your zone!");
        player->CLOSE_GOSSIP_MENU();
        return false;
}
         }
         else
         {
                 ChatHandler(player->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFThat player is not online!");
                 player->CLOSE_GOSSIP_MENU();
                 return false;
         }
 }
 else
 {
         ChatHandler(player->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFThat player was is not online!");
         player->CLOSE_GOSSIP_MENU();
         return false;
 }
 player->CLOSE_GOSSIP_MENU();
 return true;
        }
 
};
 
class BloodMoneyReward : public PlayerScript
{
 public:
         BloodMoneyReward() : PlayerScript("BloodMoneyReward") {}
         
        void OnDuelEnd(Player* winner, Player* loser, DuelCompleteType type)
        {
                if (type != DUEL_WON)
                        return;
                if (HasBloodMoneyChallenger(winner->GetGUID()) || HasBloodMoneyChallenger(loser->GetGUID()))
                {                        
                  BloodMoneyList list1 = m_bloodMoney[winner->GetGUID()];
                  BloodMoneyList list2 = m_bloodMoney[loser->GetGUID()];
 
                  BloodMoneyList::const_iterator itr;
                  for (itr = list1.begin(); itr != list1.end(); ++itr)
                  {
if (itr->guid == loser->GetGUID() && itr->accepted)
{
        if (USE_TOKEN)
        {
if (winner->GetItemCount(TOKEN_ID) < itr->amount)
{
        winner->AddAura(15007, winner);         // Apply Rez sickness for possible cheating
        ChatHandler(winner->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYou have gained Resurrection Sickness for possibly trying to abuse the system.");
        ChatHandler(loser->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYour opponent tried to cheat you. Don't worry you did not lose any tokens because of this.");
        RemoveBloodMoneyEntry(winner->GetGUID(), itr->guid);
        return;
}
if (loser->GetItemCount(TOKEN_ID) >= itr->amount)
{
        winner->AddItem(TOKEN_ID, itr->amount);
        ChatHandler(winner->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFCongratulations on winning %u tokens!", itr->amount);
        Item* item = loser->GetItemByEntry(TOKEN_ID);
        loser->DestroyItemCount(TOKEN_ID, itr->amount, true);
        RemoveBloodMoneyEntry(winner->GetGUID(), itr->guid);
}
else
{
        loser->AddAura(15007, loser);           // Apply Rez sickness for possible cheating
        ChatHandler(winner->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYour opponent tried to cheat you. He did not have enough tokens to pay off the bet.");
        ChatHandler(loser->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYou have gained Resurrection Sickness for possibly trying to abuse the system.");
        RemoveBloodMoneyEntry(winner->GetGUID(), itr->guid);
}
return;
        }
        else
        {
if (winner->GetMoney() < itr->amount)
{
        winner->AddAura(15007, winner);         // Apply Rez sickness for possible cheating
        ChatHandler(winner->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYou have gained Resurrection Sickness for possibly trying to abuse the system.");
        ChatHandler(loser->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYour opponent tried to cheat you. Don't worry you did not lose any money because of this.");
        RemoveBloodMoneyEntry(winner->GetGUID(), itr->guid);
        return;
}
if (loser->GetMoney() >= itr->amount)
{
        winner->ModifyMoney(itr->amount);
        ChatHandler(winner->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFCongratulations on winning %ug!", itr->amount/10000);
        loser->ModifyMoney(-(int32)(itr->amount));
        RemoveBloodMoneyEntry(winner->GetGUID(), itr->guid);
}
else
{
        loser->AddAura(15007, loser);           // Apply Rez sickness for possible cheating
        ChatHandler(winner->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYour opponent tried to cheat you. He did not have enough money to pay off the bet.");
        ChatHandler(loser->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYou have gained Resurrection Sickness for possibly trying to abuse the system.");
        RemoveBloodMoneyEntry(winner->GetGUID(), itr->guid);
}
return;
        }
}
  }
  for (itr = list2.begin(); itr != list2.end(); ++itr)
  {
  if (itr->guid == winner->GetGUID() && itr->accepted)
  {
 if (USE_TOKEN)
 {
         if (winner->GetItemCount(TOKEN_ID) < itr->amount)
         {
                 winner->AddAura(15007, winner);         // Apply Rez sickness for possible cheating
                 ChatHandler(winner->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYou have gained Resurrection Sickness for possibly trying to abuse the system.");
                 ChatHandler(loser->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYour opponent tried to cheat you. Don't worry you did not lose any tokens because of this.");
                 RemoveBloodMoneyEntry(loser->GetGUID(), itr->guid);
                 return;
         }
         if (loser->GetItemCount(TOKEN_ID) >= itr->amount)
         {
                 winner->AddItem(TOKEN_ID, itr->amount);
                 ChatHandler(winner->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFCongratulations on winning %u tokens!", itr->amount);
                 Item* item = loser->GetItemByEntry(TOKEN_ID);
                 loser->DestroyItemCount(TOKEN_ID, itr->amount, true);
                 RemoveBloodMoneyEntry(loser->GetGUID(), itr->guid);
         }
         else
         {
                 loser->AddAura(15007, loser);           // Apply Rez sickness for possible cheating
                 ChatHandler(winner->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYour opponent tried to cheat you. He did not have enough tokens to pay off the bet.");
                 ChatHandler(loser->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYou have gained Resurrection Sickness for possibly trying to abuse the system.");
                 RemoveBloodMoneyEntry(loser->GetGUID(), itr->guid);
         }
         return;
 }
 else
 {
 if (winner->GetMoney() < itr->amount)
 {
         winner->AddAura(15007, winner);         // Apply Rez sickness for possible cheating
         ChatHandler(winner->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYou have gained Resurrection Sickness for possibly trying to abuse the system.");
         ChatHandler(loser->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYour opponent tried to cheat you. Don't worry you did not lose any money because of this.");
         RemoveBloodMoneyEntry(loser->GetGUID(), itr->guid);
         return;
 }
 if (loser->GetMoney() >= itr->amount)
 {
         winner->ModifyMoney(itr->amount);
         ChatHandler(winner->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFCongratulations on winning %ug!", itr->amount/10000);
         loser->ModifyMoney(-(int32)(itr->amount));
         RemoveBloodMoneyEntry(loser->GetGUID(), itr->guid);
 }
 else
 {
         loser->AddAura(15007, loser);           // Apply Resurrection sickness for possible cheating
         ChatHandler(winner->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYour opponent tried to cheat you. He did not have enough money to pay off the bet.");
         ChatHandler(loser->GetSession()).PSendSysMessage("[Duel Challenge] |cffFFFFFFYou have gained Resurrection Sickness for possibly trying to abuse the system.");
         RemoveBloodMoneyEntry(loser->GetGUID(), itr->guid);
 }
 return;
                                        }
                                }
                        }
 
                 }
         }
};
 
void AddSC_npc_blood_money()
{
        new BloodMoneyReward();
        new npc_blood_money();
}