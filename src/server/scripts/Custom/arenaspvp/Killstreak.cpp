//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"

bool PvPSystemEnabled = true;

struct SystemInfo
{
	uint32 KillStreak;
	uint32 LastGUIDKill;
};

static std::map<uint32, SystemInfo> KillingStreak;

class System_OnKill : public PlayerScript
{
public:
	System_OnKill() : PlayerScript("System_OnPVPKill") {}

	void OnPVPKill(Player *pKiller, Player *pVictim)
	{

		if (PvPSystemEnabled == false)
		{
			return;
		}

		else if (PvPSystemEnabled == true)
		{
			uint32 kGUID;
			uint32 vGUID;
			kGUID = pKiller->GetGUID();
			vGUID = pVictim->GetGUID();

			if (KillingStreak[vGUID].KillStreak >= 3)
			{
				std::ostringstream ss; 
				ss << "|TInterface/ICONS/Achievement_bg_most_damage_killingblow_dieleast:20|t|CFF7DA7F0" << pKiller->GetName() << "|r has ended |CFFFAA423" << pVictim->GetName() << "|r kill streak!!!|CFFffff00";
				sWorld->SendServerMessage(SERVER_MSG_STRING, ss.str().c_str());
			}

			if (kGUID == vGUID || KillingStreak[kGUID].LastGUIDKill == kGUID)
				return;

			KillingStreak[kGUID].KillStreak++;
			KillingStreak[vGUID].KillStreak = 0;
			KillingStreak[kGUID].LastGUIDKill = vGUID;
			KillingStreak[vGUID].LastGUIDKill = 0;

			if (KillingStreak[kGUID].KillStreak % 5 == 0) 
			{
				std::ostringstream ss;
				ss << "|TInterface/ICONS/Achievement_bg_kill_carrier_opposing_flagroom:20|tCaution! |CFF7DA7F0" << pKiller->GetName() << "|r the criminal who is quickly rising to the top with |CFF7DA7F0" << KillingStreak[kGUID].KillStreak << "|r kills!";
				sWorld->SendServerMessage(SERVER_MSG_STRING, ss.str().c_str());
				pKiller->CastSpell(pKiller, 108124, true); //Temporal Aura
				pKiller->ModifyCurrency(CURRENCY_TYPE_HONOR_POINTS, 2490); //50 Honor every 5 kills steaks
			}

			else if (KillingStreak[kGUID].KillStreak == 3)
			{
				std::ostringstream ss;
				ss << "|cFF81CF42" << pKiller->GetName() << "|r has drew first blood!";
				sWorld->SendServerMessage(SERVER_MSG_STRING, ss.str().c_str());
			}		
		}
	}
};

void AddSC_PvP_System()
{
	new System_OnKill;
}