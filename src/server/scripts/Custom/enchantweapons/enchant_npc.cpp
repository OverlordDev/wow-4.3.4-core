//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"

enum WeaponEnchants
{
	ENCHANT_WEP_PYRIUM_WEAPON_CHAIN  = 4217,	//1
	ENCHANT_WEP_LANDSLIDE            = 4099,	//2
	ENCHANT_WEP_POWER_TORRENT        = 4097,	//3
	ENCHANT_WEP_WINDWALK             = 4098,	//4
	ENCHANT_WEP_HEARTSONG            = 4084,	//5
	ENCHANT_WEP_HURRICANE            = 4083,	//6
	ENCHANT_WEP_ELEMENTAL_SLAYER     = 4074,	//7
	ENCHANT_WEP_MONGOOSE             = 2673,	//8
	ENCHANT_WEP_MENDING              = 4066,	//9
	ENCHANT_2HWEP_MIGHTY_AGILITY     = 4227,	//10

	// Deathknight Enchants

	ENCHANT_WEP_CINDERGLACIER        = 3369,	//11
	ENCHANT_WEP_LICHBANE             = 3366,	//12
	ENCHANT_WEP_RAZORICE             = 3370,	//13
	ENCHANT_WEP_SPELLBREAKING        = 3595,	//14
	ENCHANT_WEP_SWORDBREAKING        = 3594,	//15
	ENCHANT_2HWEP_SWORDSHATTERING    = 3365,	//16
	ENCHANT_WEP_FALLEN_CRUSADER      = 3368,	//17
	ENCHANT_WEP_NERUBIAN_CARAPACE    = 3883,	//18
	ENCHANT_2HWEP_STONESKIN_GARG     = 3847,    //19

	//Test Enchants
	EXECUTIONER						 = 3225		//20 

};
class transmog_npc : public CreatureScript
{
public:
	transmog_npc() : CreatureScript("transmog_npc") { }

	bool OnGossipHello(Player * player, Creature * creature)
	{
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra:20|tEnchant ->|CFFe1c349Pyrium Weapon Chain|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra_blue:20|tEnchant ->|CFF4d61b8Landslide|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra_green:20|tEnchant ->|CFF4ea524Power Torrent|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra_red:20|tEnchant ->|CFFdb2120Windwalk|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra:20|tEnchant ->|CFFe1c349Heartsong|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 5);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra_blue:20|tEnchant ->|CFF4d61b8Hurricane|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 6);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra_green:20|tEnchant ->|CFF4ea524Elemental Slayer|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 7);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra_red:20|tEnchant ->|CFFdb2120Mongoose|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 8);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra:20|tEnchant ->|CFFe1c349Mending|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 9);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra_blue:20|tEnchant ->|CFF4d61b8Mighty Agility|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 10);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra_green:20|tEnchant ->|CFF4ea524Cinderglacier|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 11);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra_red:20|tEnchant ->|CFFdb2120Lichbane|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 12);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra:20|tEnchant ->|cFFe1c349Razorice|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 13);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra_blue:20|tEnchant ->|CFF4d61b8Spellbreaking|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 14);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra_green:20|tEnchant ->|CFF4ea524Swordbreaking|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 15);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra_red:20|tEnchant ->|CFFdb2120SwordShattering|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 16);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra:20|tEnchant ->|CFFe1c349Fallen Crusader|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 17);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra_blue:20|tEnchant ->|CFF4d61b8Nerubian Carapace|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 18);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra_green:20|tEnchant ->|CFF4ea524Stoneskin Gargoyle|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 19);
		player->ADD_GOSSIP_ITEM(-1, "|TInterface\\icons\\Priest_icon_chakra_red:20|tEnchant ->|CFFdb2120Executioner|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 20);


		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());

		return true;

	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		if (sender != GOSSIP_SENDER_MAIN)
			return false;

		switch (action)
		{
		case GOSSIP_ACTION_INFO_DEF + 1:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_WEP_PYRIUM_WEAPON_CHAIN);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_WEP_PYRIUM_WEAPON_CHAIN);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 2:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_WEP_LANDSLIDE);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_WEP_LANDSLIDE);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 3:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_WEP_POWER_TORRENT);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_WEP_POWER_TORRENT);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 4:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_WEP_WINDWALK);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_WEP_WINDWALK);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 5:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_WEP_HEARTSONG);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_WEP_HEARTSONG);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 6:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_WEP_HURRICANE);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_WEP_HURRICANE);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 7:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_WEP_ELEMENTAL_SLAYER);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_WEP_ELEMENTAL_SLAYER);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 8:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_WEP_MONGOOSE);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_WEP_MONGOOSE);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 9:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_WEP_MENDING);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_WEP_MENDING);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 10:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_2HWEP_MIGHTY_AGILITY);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_2HWEP_MIGHTY_AGILITY);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 11:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_WEP_CINDERGLACIER);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_WEP_CINDERGLACIER);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 12:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_WEP_LICHBANE);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_WEP_LICHBANE);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 13:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_WEP_RAZORICE);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_WEP_RAZORICE);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 14:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_WEP_SPELLBREAKING);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_WEP_SPELLBREAKING);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 15:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_WEP_SWORDBREAKING);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_WEP_SWORDBREAKING);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 16:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_2HWEP_SWORDSHATTERING);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_2HWEP_SWORDSHATTERING);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 17:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_WEP_FALLEN_CRUSADER);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_WEP_FALLEN_CRUSADER);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 18:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_WEP_NERUBIAN_CARAPACE);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_WEP_NERUBIAN_CARAPACE);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 19:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, ENCHANT_2HWEP_STONESKIN_GARG);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), ENCHANT_2HWEP_STONESKIN_GARG);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		case GOSSIP_ACTION_INFO_DEF + 20:
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult Itemguid = CharacterDatabase.PQuery("SELECT Itemguid FROM weapon_enchant_transmog WHERE Itemguid = '%u'", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				ReplacingPreparedStatement(player, EXECUTIONER);
				player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), EXECUTIONER);
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("Enchantment Successfull");
			}
			else
			{
				player->PlayerTalkClass->SendCloseGossip();
				player->GetSession()->SendNotification("You must equip a weapon!");
			}
			break;
		}
		return true;
	}

	void ReplacingPreparedStatement(Player* player, uint32 enchant)
	{
		PreparedStatement * stmt = CharacterDatabase.GetPreparedStatement(CHAR_REP_WEAPON_ENCHANT_TRANSMOG);
		stmt->setUInt32(0, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
		stmt->setUInt32(1, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetEnchantmentId(PERM_ENCHANTMENT_SLOT));
		stmt->setUInt32(2, enchant);
		CharacterDatabase.Execute(stmt);
	}
};

void AddSC_transmog_npc()
{
	new transmog_npc();
}