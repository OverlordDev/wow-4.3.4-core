//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"

enum TriggerSpell
{
	PING_TRIGGER_SPELL = 97927
};
class set_enchant : public PlayerScript
{
public : 
	set_enchant() : PlayerScript("set_enchant") { }
 
	void OnSpellCast(Player* player, Spell* spellid, bool skipCheck)
	{
		if (spellid->GetSpellInfo()->Id == PING_TRIGGER_SPELL)
		{
			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
			{
				QueryResult ResultMain = CharacterDatabase.PQuery("SELECT CustomEnchant FROM weapon_enchant_transmog WHERE Itemguid = '%u' ", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND)->GetGUID());
				if (ResultMain)
				{
					uint32 EnchantID = ResultMain->Fetch()->GetInt32();
					player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_MAINHAND * 2), EnchantID);
				}
			}

			if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND))
			{
				QueryResult ResultOff = CharacterDatabase.PQuery("SELECT CustomEnchant FROM weapon_enchant_transmog WHERE Itemguid = '%u' ", player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND)->GetGUID());
				if (ResultOff)
				{
					uint32 EnchantID = ResultOff->Fetch()->GetInt32();
					player->SetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENCHANTMENT + (EQUIPMENT_SLOT_OFFHAND * 2), EnchantID);
				}
			}
		}
	}
};

void AddSC_set_enchant()
{
	new set_enchant();
}