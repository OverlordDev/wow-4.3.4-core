//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"

class Exchanger_NPC : public CreatureScript
{
public:
    Exchanger_NPC() : CreatureScript("Exchanger_NPC") { }
 
    bool OnGossipHello(Player *player, Creature * creature)
    {
        if (player->isInCombat())
        {
            player->CLOSE_GOSSIP_MENU();
            creature->MonsterWhisper("You are in combat already!", player->GetGUID());
            return true;
        }
        else
        {
            player->ADD_GOSSIP_ITEM_EXTENDED(5, "|TInterface\\icons\\Pvpcurrency-conquest-horde:40|t Exchange 300 Conquest for 200 Valor", GOSSIP_SENDER_MAIN, 1, "Are you sure you want to exchange 300 Conquest points for 200 Valor points?", 0, false);
            player->ADD_GOSSIP_ITEM_EXTENDED(5, "|TInterface\\icons\\Pvecurrency-valor:40|t Exchange 200 Valor for 300 Conquest", GOSSIP_SENDER_MAIN, 2, "Are you sure you want to exchange 200 Valor points for 300 Conquest points?", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|TInterface\\icons\\Pvpcurrency-honor-horde:40|t Exchange 1k Honor for 100 Conquest", GOSSIP_SENDER_MAIN, 3, "Are you sure you want to exchange 1000 Honor points for 100 Conquest points?", 0, false);
        }
 
        player->SEND_GOSSIP_MENU(60040, creature->GetGUID());
        return true;
    }
 
    bool OnGossipSelect(Player * player, Creature * creature, uint32 sender, uint32 uiAction)
    {
        if (sender == GOSSIP_SENDER_MAIN)
        {
            player->PlayerTalkClass->ClearMenus();
            uint32 valor = player->GetCurrency(CURRENCY_TYPE_VALOR_POINTS, true);
			uint32 conquest = player->GetCurrency(CURRENCY_TYPE_CONQUEST_POINTS, true);
			uint32 honor = player->GetCurrency(CURRENCY_TYPE_HONOR_POINTS, true);
            switch (uiAction)
            {
            case 1:
                /*Note: Normally the value you use its value / 100 = ingame value but if we use * CURRENCY_PRECISION it will use exactly that amount*/
                if (conquest)
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->ModifyCurrency(CURRENCY_TYPE_CONQUEST_POINTS, -300 * CURRENCY_PRECISION, true, true); 
					player->ModifyCurrency(CURRENCY_TYPE_VALOR_POINTS, +200 * CURRENCY_PRECISION, true, true); 
                    creature->MonsterWhisper("|c00077766|r You have exchanged 300 Conquest points for 200 Valor points successfully!", player->GetGUID(), true);
 
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    creature->MonsterWhisper("|c00077766|r You don't have enough Conquest points!", player->GetGUID(), true);
 
                    return false;
                }
                break;
 
            case 2:
                if (valor)
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->ModifyCurrency(CURRENCY_TYPE_CONQUEST_POINTS, +300 * CURRENCY_PRECISION, true, true); 
					player->ModifyCurrency(CURRENCY_TYPE_VALOR_POINTS, -200 * CURRENCY_PRECISION, true, true); 
                    creature->MonsterWhisper("|c00077766|r You exchanged 200 Valor points for 300 Conquest points successfully!", player->GetGUID(), true);
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    creature->MonsterWhisper("|c00077766|r You don't have enough Valor points!", player->GetGUID(), true);
                    return false;
                }
                break;

			case 3:
				if (honor)
				{
					player->CLOSE_GOSSIP_MENU();
					player->ModifyCurrency(CURRENCY_TYPE_CONQUEST_POINTS, +100 * CURRENCY_PRECISION, true, true);
					player->ModifyCurrency(CURRENCY_TYPE_HONOR_POINTS, -1000 * CURRENCY_PRECISION, true, true);
					creature->MonsterWhisper("|c00077766|r You exchanged 1000 Honor points for 100 Conquest points successfully!", player->GetGUID(), true);
				}
				else
				{
					player->CLOSE_GOSSIP_MENU();
					creature->MonsterWhisper("|c00077766|r You don't have enough Honor points!", player->GetGUID(), true);
					return false;
				}
				break;
            }
        }
        return true;
    }
};
 
void AddSC_Exchanger_NPC()
{
    new Exchanger_NPC();
}