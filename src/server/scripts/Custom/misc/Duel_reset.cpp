//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"
#include "Config.h"
#include "World.h"

class Duel_Reset : public PlayerScript
{
    public:
		Duel_Reset() : PlayerScript("DuelResetCooldown") {}

	// Call the script after the 3s countdown
	void OnDuelStart(Player * player, Player * plTarget)
	{
		/*// Reset both players cooldowns
		player->RemoveAllSpellCooldown();    
		plTarget->RemoveAllSpellCooldown(); Can be abused*/

		/*// Paladin Debuff Spells On Duel Start
		player->RemoveAura(1022); // Hand of Protection
		plTarget->RemoveAura(1022); 
		player->RemoveAura(498); // Divine Protection
		plTarget->RemoveAura(498);
		player->RemoveAura(642); // Divine Shield
		plTarget->RemoveAura(642);
		player->RemoveAura(31850); // Ardent Defender Debuff
		plTarget->RemoveAura(31850);
		player->RemoveAura(31884); // Avenging Wrath
		plTarget->RemoveAura(31884);*/

		// Paladin Debuff Spells On Duel Start
		player->RemoveAura(25771); // Forbearance Debuff
		plTarget->RemoveAura(25771);

		// Mage Debuff Spells On Duel Start
		player->RemoveAura(41425); // Remove Hypothermia Debuff
		plTarget->RemoveAura(41425);

		// Shaman Debuff Spells On Duel Start
		player->RemoveAura(57724); // Remove Sated Debuff (Horde)
		plTarget->RemoveAura(57724);
		player->RemoveAura(57723); // Remove Exhaustion Debuff (Alliance)
		plTarget->RemoveAura(57723);

		// Professions and Trinkets 
		player->RemoveAura(11196);// Recently Bandaged Debuff - Profession
		plTarget->RemoveAura(11196);

		// Restore Health and Mana
		player->SetHealth(player->GetMaxHealth());
		player->SetPower(POWER_MANA, player->GetMaxPower(POWER_MANA));
		plTarget->SetHealth(plTarget->GetMaxHealth());
		plTarget->SetPower(POWER_MANA,  plTarget->GetMaxPower(POWER_MANA));

		// Remove Rage and Runic Power
		player->SetPower(POWER_RAGE, 0);       
		plTarget->SetPower(POWER_RAGE, 0);
		player->SetPower(POWER_RUNIC_POWER, 0);
		plTarget->SetPower(POWER_RUNIC_POWER, 0);
	}

    void OnDuelEnd(Player* winner, Player* loser, DuelCompleteType type)
    {
        // Reset players cooldown
		if ((sConfigMgr->GetIntDefault("DuelResetCooldown.Enable", 1)) &&
			(winner->GetAreaId() == (sConfigMgr->GetIntDefault("DuelReset.Area.One", NULL)) || winner->GetAreaId() == (sConfigMgr->GetIntDefault("DuelReset.Area.Two", NULL))))
        {
            winner->ResetAllPowers();
			winner->ClearDiminishings();
            winner->RemoveArenaSpellCooldowns(true);
			
            loser->ResetAllPowers();
			loser->ClearDiminishings();
            loser->RemoveArenaSpellCooldowns(true);	

			// Paladin Debuff Spells On Duel Start
			winner->RemoveAura(25771); // Forbearance Debuff
			loser->RemoveAura(25771);
	
			// Mage Debuff Spells On Duel Start
			winner->RemoveAura(41425); // Remove Hypothermia Debuff
			loser->RemoveAura(41425);
			winner->RemoveAura(87023); // Remove Cauterize Debuff
			loser->RemoveAura(87023);
	
			// Shaman Debuff Spells On Duel Start
			winner->RemoveAura(57724); // Remove Sated Debuff (Horde)
			loser->RemoveAura(57724);
			winner->RemoveAura(57723); // Remove Exhaustion Debuff (Alliance)
			loser->RemoveAura(57723);			
        }
    }
};

void AddSC_Duel_Reset()
{
	new Duel_Reset();
}