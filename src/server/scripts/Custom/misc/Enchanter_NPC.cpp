﻿//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"
 
enum Enchants
{
    ENCHANT_WEP_PYRIUM_WEAPON_CHAIN = 4217,
    ENCHANT_WEP_LANDSLIDE = 4099,
    ENCHANT_WEP_POWER_TORRENT = 4097,
    ENCHANT_WEP_WINDWALK = 4098,
    ENCHANT_WEP_HEARTSONG = 4084,
    ENCHANT_WEP_HURRICANE = 4083,
    ENCHANT_WEP_ELEMENTAL_SLAYER = 4074,
	ENCHANT_WEP_MONGOOSE = 2673,
    //ENCHANT_WEP_MENDING = 4066,
    ENCHANT_2HWEP_MIGHTY_AGILITY = 4227,
 
    ENCHANT_OFF_HAND_SUPERIOR_INTELLECT = 4091,
    ENCHANT_SHIELD_MASTERY = 4085,
    ENCHANT_SHIELD_PROTECTION = 4073,
 
    ENCHANT_HEAD_VICIOUS_INTELLECT = 4245,
    ENCHANT_HEAD_VICIOUS_AGILITY = 4246,
    ENCHANT_HEAD_VICIOUS_STRENGTH = 4247,
	ENCHANT_HEAD_UNBREAKABLE_QUARTZ = 4198,
	ENCHANT_HEAD_CHARGED_LODESTONE = 4200,
	ENCHANT_HEAD_JAGGED_STONE = 4202,
	ENCHANT_HEAD_SHATTERED_CRYSTAL = 4204,
 
    ENCHANT_SHOULDER_VICIOUS_INTELLECT = 4248,
    ENCHANT_SHOULDER_VICIOUS_AGILITY = 4250,
    ENCHANT_SHOULDER_VICIOUS_STRENGTH = 4249,
	ENCHANT_SHOULDER_UNBREAKABLE_QUARTZ = 4198,
	ENCHANT_SHOULDER_CHARGED_LODESTONE = 4200,
	ENCHANT_SHOULDER_JAGGED_STONE = 4202,
	ENCHANT_SHOULDER_SHATTERED_CRYSTAL = 4204,
 
	ENCHANT_CLOAK_MAJOR_AGILITY = 1099,
    ENCHANT_CLOAK_GREATER_CRITICAL_STRIKE = 4100,
    ENCHANT_CLOAK_GREATER_INTELLECT = 4096,
    ENCHANT_CLOAK_PROTECTION = 4090,
    ENCHANT_CLOAK_GREATER_SPELL_PIERCING = 4064,
 
	ENCHANT_GLOVES_AGILITY = 3222,
    ENCHANT_GLOVES_GREATER_MASTERY = 4107,
    ENCHANT_GLOVES_MIGHTY_STRENGTH = 4106,
    ENCHANT_GLOVES_GREATER_EXPERTISE = 4082,
    ENCHANT_GLOVES_HASTE = 4068,
 
    ENCHANT_BRACERS_GREATER_CRITICAL_STRIKE = 4101,
    ENCHANT_BRACERS_GREATER_SPEED = 4108,
    ENCHANT_BRACERS_AGILITY = 4258,
    ENCHANT_BRACERS_MAJOR_STRENGTH = 4256,
    ENCHANT_BRACERS_MIGHTY_INTELLECT = 4257,
    ENCHANT_BRACERS_GREATER_EXPERTISE = 4095,
    ENCHANT_BRACERS_EXCEPTIONAL_SPIRIT = 4093,
    ENCHANT_BRACERS_PRECISION = 4089,
    ENCHANT_BRACERS_SUPERIOR_DODGE = 4086,
 
    ENCHANT_CHEST_PEERLESS_STATS = 4102,
    ENCHANT_CHEST_GREATER_STAMINA = 4103,
    ENCHANT_CHEST_EXCEPTIONAL_SPIRIT = 4088,
    ENCHANT_CHEST_MIGHTY_RESILIENCE = 4077,
 
    ENCHANT_LEGS_LEG_ARMOR = 4127,
    ENCHANT_DRAKEHIDE_LEG_ARMOR = 4270,
    ENCHANT_LEGS_CHARSCALE_LEG_ARMOR = 4127,
    ENCHANT_LEGS_DRAGONSCALE_LEG_ARMOR = 4126,
    ENCHANT_LEGS_POWERFUL_ENCHANTED_SPELLTHREAD = 4112,
    ENCHANT_LEGS_POWERFUL_GHOSTLY_SPELLTHREAD = 4110,
 
    ENCHANT_BOOTS_ASSASSINS_STEP = 4105,
    ENCHANT_BOOTS_LAVAWALKER = 4104,
    ENCHANT_BOOTS_MASTERY = 4094,
    ENCHANT_BOOTS_PRECISION = 4092,
    ENCHANT_BOOTS_MAJOR_AGILITY = 4076,
    ENCHANT_BOOTS_HASTE = 4069,
 
    ENCHANT_RING_AGILITY = 4079,
    ENCHANT_RING_GREATER_STAMINA = 4081,
    ENCHANT_RING_INTELLECT = 4080,
    ENCHANT_RING_STRENGTH = 4078,

	ENCHANT_BELT_SOCKET = 3729,

	ENCHANT_RANGED_AGILITY = 4267
};
 
#define ONE_HAND_WEAPON_COST    0 //20g
#define TWO_HAND_WEAPON_COST    0 //30g
#define MAIN_PIECES_COST        0 //15g
#define OFF_PIECES_COST         0 //10g
#define SHIELD_COST             0 //10g

void AdvancedEnchant(Player* player, Item* item, EnchantmentSlot slot, uint32 socketGem)
{
	if (!item)
	{
		player->GetSession()->SendNotification("Equip an item first!");
		return;
	}

	if (!socketGem)
	{
		player->GetSession()->SendNotification("Something went wrong in the code. It has been logged for developers and will be looked into, sorry for the inconvenience.");
		return;
	}

	player->ApplyEnchantment(item, slot, false);
	item->SetEnchantment(slot, socketGem, 0, 0);
	player->ApplyEnchantment(item, slot, true);

	std::string color = "|cff";
	switch (item->GetTemplate()->Quality)
	{
	case 0:
		color += "9d9d9d";
		break;
	case 1:
		color += "ffffff";
		break;
	case 2:
		color += "1eff00";
		break;
	case 3:
		color += "0070dd";
		break;
	case 4:
		color += "a335ee";
		break;
	case 5:
		color += "ff8000";
		break;
	}
	std::string itemname = item->GetTemplate()->Name1;
	ChatHandler(player->GetSession()).PSendSysMessage("|cff81F7F3Your |cffFFFFFF[%s%s|cffFFFFFF] |cff81F7F3has been boosted up!", color.c_str(), itemname.c_str());
}
 
void Enchant(Player* player, Item* item, uint32 enchantid)
{
    if (!item)
    {
        player->GetSession()->SendNotification("Equip an item first!");
        return;
    }
 
    if (!enchantid)
        return;
 
    player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, false);
    item->SetEnchantment(PERM_ENCHANTMENT_SLOT, enchantid, 0, 0);
    player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, true);
 
    std::string color = "|cff";
    switch (item->GetTemplate()->Quality)
    {
    case 0:
        color += "9d9d9d";
        break;
    case 1:
        color += "ffffff";
        break;
    case 2:
        color += "1eff00";
        break;
    case 3:
        color += "0070dd";
        break;
    case 4:
        color += "a335ee";
        break;
    case 5:
        color += "ff8000";
        break;
    }
    std::string itemname = item->GetTemplate()->Name1;
    ChatHandler(player->GetSession()).PSendSysMessage("|cff81F7F3Your |cffFFFFFF[%s%s|cffFFFFFF] |cff81F7F3has been boosted up!", color.c_str(), itemname.c_str());
}
 
class npc_enchantment : public CreatureScript
{
public:
    npc_enchantment() : CreatureScript("npc_enchantment") { }
 
    bool OnGossipHello(Player* player, Creature* creature)
    {
        player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_axe_1h_deathwingraiddw_d_01:25|t Enchant Mainhand Weapon", GOSSIP_SENDER_MAIN, 1);
		player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_axe_1h_deathwingraiddw_d_01:25|t Enchant Offhand Weapon & Shield", GOSSIP_SENDER_MAIN, 3);
        player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_axe_68:25|t Enchant Two Hands Weapon", GOSSIP_SENDER_MAIN, 2);
		player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Bow_1h_pvp400_c_01:25|t Enchant Ranged Weapon", GOSSIP_SENDER_MAIN, 14);
        player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_helmet_78:25|t Enchant Helmet", GOSSIP_SENDER_MAIN, 4);
        player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_shoulder_90:25|t Enchant Shoulders", GOSSIP_SENDER_MAIN, 5);
        player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_misc_cape_22:25|t Enchant Cloak", GOSSIP_SENDER_MAIN, 6);
        player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_chest_plate23:25|t Enchant Armor", GOSSIP_SENDER_MAIN, 7);
        player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_bracer_25b:25|t Enchant Bracers", GOSSIP_SENDER_MAIN, 8);
        player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_gauntlets_robe_raidpriest_j_01:25|t Enchant Gloves", GOSSIP_SENDER_MAIN, 9);
		player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_belt_02:25|t Enchant Belt", GOSSIP_SENDER_MAIN, 13);
        player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_pants_robe_raidpriest_j_01:25|t Enchant Pants", GOSSIP_SENDER_MAIN, 11);
        player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_boots_cloth_16:25|t Enchant Boots", GOSSIP_SENDER_MAIN, 10);
 
        if (player->HasSkill(SKILL_ENCHANTING))
            player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_jewelry_ring_78:25|t Enchant Rings", GOSSIP_SENDER_MAIN, 12);
 
        player->SEND_GOSSIP_MENU(100001, creature->GetGUID());
        return true;
    }
 
    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
    {
        player->PlayerTalkClass->ClearMenus();
        Item * mainItem;
 
        switch (action)
        {
 
        case 1: // Enchant Weapon
            player->ADD_GOSSIP_ITEM(1, "'-50% Disarm Effect'", GOSSIP_SENDER_MAIN, 217);
			player->ADD_GOSSIP_ITEM(1, "'1000 Attack Power proc'", GOSSIP_SENDER_MAIN, 104);
			player->ADD_GOSSIP_ITEM(1, "'500 Intellect proc'", GOSSIP_SENDER_MAIN, 106);
            player->ADD_GOSSIP_ITEM(1, "'200 Spirit proc'", GOSSIP_SENDER_MAIN, 102);
            player->ADD_GOSSIP_ITEM(1, "'450 Haste proc'", GOSSIP_SENDER_MAIN, 103);
			player->ADD_GOSSIP_ITEM(1, "'Arcane Damage & Silence'", GOSSIP_SENDER_MAIN, 101);
            player->ADD_GOSSIP_ITEM(1, "'600 Dodge & 15% Mov. Speed proc'", GOSSIP_SENDER_MAIN, 107);
			player->ADD_GOSSIP_ITEM(1, "'Mongoose 120 Agility proc'", GOSSIP_SENDER_MAIN, 100);
			//player->ADD_GOSSIP_ITEM(1, "Mending - Heal proc", GOSSIP_SENDER_MAIN, 105);
            player->ADD_GOSSIP_ITEM(1, "<-Return", GOSSIP_SENDER_MAIN, 300);
            player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
            return true;
            break;
 
        case 2: // Enchant 2H Weapon
        {
            mainItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);
            if (!mainItem || mainItem->GetTemplate()->InventoryType != INVTYPE_2HWEAPON)
            {
                player->GetSession()->SendAreaTriggerMessage("|cffFF0000Equip a two hands weapons first!");
                return false;
                player->CLOSE_GOSSIP_MENU();;
            }
            if (mainItem->GetTemplate()->InventoryType == INVTYPE_2HWEAPON)
            {
				player->ADD_GOSSIP_ITEM(1, "'-50% Disarm Effect'", GOSSIP_SENDER_MAIN, 217);
				player->ADD_GOSSIP_ITEM(1, "'1000 Attack Power proc'", GOSSIP_SENDER_MAIN, 104);
				player->ADD_GOSSIP_ITEM(1, "'+130 Agility'", GOSSIP_SENDER_MAIN, 108);
				player->ADD_GOSSIP_ITEM(1, "'500 Intellect proc'", GOSSIP_SENDER_MAIN, 106);
                player->ADD_GOSSIP_ITEM(1, "'200 Spirit proc'", GOSSIP_SENDER_MAIN, 102);
                player->ADD_GOSSIP_ITEM(1, "'450 Haste proc'", GOSSIP_SENDER_MAIN, 103);
                player->ADD_GOSSIP_ITEM(1, "'Arcane Damage & Silence'", GOSSIP_SENDER_MAIN, 101);
                player->ADD_GOSSIP_ITEM(1, "'600 Dodge & 15% Mov. Speed proc'", GOSSIP_SENDER_MAIN, 107);
				player->ADD_GOSSIP_ITEM(1, "'Mongoose 120 Agility proc'", GOSSIP_SENDER_MAIN, 100);
				//player->ADD_GOSSIP_ITEM(1, "Mending - Heal", GOSSIP_SENDER_MAIN, 105);
                player->ADD_GOSSIP_ITEM(1, "<-Return", GOSSIP_SENDER_MAIN, 300);
                player->PlayerTalkClass->SendGossipMenu(100003, creature->GetGUID());
                return true;
            }
        }
            break;
 
        case 3: // Enchant Shield
        {
            mainItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);
            if (!mainItem)
            {
                player->GetSession()->SendAreaTriggerMessage("|cffFF0000Equip a offhand weapon or shield first!");
                player->CLOSE_GOSSIP_MENU();
                return false;
            }
            if (mainItem->GetTemplate()->InventoryType == INVTYPE_SHIELD)
            {
                player->ADD_GOSSIP_ITEM(1, "'50 Mastery'", GOSSIP_SENDER_MAIN, 109);
				player->ADD_GOSSIP_ITEM(1, "'40 Intellect'", GOSSIP_SENDER_MAIN, 111);
                player->ADD_GOSSIP_ITEM(1, "'160 Armor'", GOSSIP_SENDER_MAIN, 110);
                player->ADD_GOSSIP_ITEM(1, "<-Return", GOSSIP_SENDER_MAIN, 300);
                player->PlayerTalkClass->SendGossipMenu(100004, creature->GetGUID());
                return true;
            }
            if (mainItem->GetTemplate()->InventoryType == INVTYPE_HOLDABLE)
            {
                player->ADD_GOSSIP_ITEM(1, "'40 Intellect'", GOSSIP_SENDER_MAIN, 111);
                player->ADD_GOSSIP_ITEM(1, "<-Return", GOSSIP_SENDER_MAIN, 300);
                player->SEND_GOSSIP_MENU(100004, creature->GetGUID());
                return true;
            }
 
        }
            break;

		case 14: // Enchant Ranged Weapon
        {
            mainItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_RANGED);
            if (!mainItem || mainItem->GetTemplate()->InventoryType != INVTYPE_RANGED)
            {
                player->GetSession()->SendAreaTriggerMessage("|cffFF0000Equip a bow or a gun first!");
                return false;
                player->CLOSE_GOSSIP_MENU();;
            }
            if (mainItem->GetTemplate()->InventoryType == INVTYPE_RANGED)
            {
				player->ADD_GOSSIP_ITEM(1, "'Proc +300 Agility'", GOSSIP_SENDER_MAIN, 174);
                player->ADD_GOSSIP_ITEM(1, "<-Return", GOSSIP_SENDER_MAIN, 300);
                player->PlayerTalkClass->SendGossipMenu(100003, creature->GetGUID());
                return true;
            }
        }
            break;
 
        case 4: // Enchant Head
            player->ADD_GOSSIP_ITEM(1, "'60 Agility & 35 Resilience'", GOSSIP_SENDER_MAIN, 112);
            player->ADD_GOSSIP_ITEM(1, "'60 Intellect & 35 Resilience'", GOSSIP_SENDER_MAIN, 113);
            player->ADD_GOSSIP_ITEM(1, "'60 Strength & 35 Resilience'", GOSSIP_SENDER_MAIN, 114);
			player->ADD_GOSSIP_ITEM(1, "'75 Stamina & 25 Dodge'", GOSSIP_SENDER_MAIN, 155);
			player->ADD_GOSSIP_ITEM(1, "'50 Intellect & 25 Haste'", GOSSIP_SENDER_MAIN, 156);
			player->ADD_GOSSIP_ITEM(1, "'50 Strenght & 25 Critical'", GOSSIP_SENDER_MAIN, 157);
			player->ADD_GOSSIP_ITEM(1, "'50 Agility & 25 Mastery'", GOSSIP_SENDER_MAIN, 158);
            player->ADD_GOSSIP_ITEM(1, "<-Return", GOSSIP_SENDER_MAIN, 300);
            player->PlayerTalkClass->SendGossipMenu(100005, creature->GetGUID());
            return true;
            break;
 
        case 5: // Enchant Shoulders
            player->ADD_GOSSIP_ITEM(1, "'50 Agility & 25 Resilience'", GOSSIP_SENDER_MAIN, 115);
            player->ADD_GOSSIP_ITEM(1, "'50 Intellect & 25 Resilience'", GOSSIP_SENDER_MAIN, 116);
            player->ADD_GOSSIP_ITEM(1, "'50 Strength & 25 Resilience'", GOSSIP_SENDER_MAIN, 117);
			player->ADD_GOSSIP_ITEM(1, "'75 Stamina & 25 Dodge'", GOSSIP_SENDER_MAIN, 170);
			player->ADD_GOSSIP_ITEM(1, "'50 Intellect & 25 Haste'", GOSSIP_SENDER_MAIN, 171);
			player->ADD_GOSSIP_ITEM(1, "'50 Strenght & 25 Critical'", GOSSIP_SENDER_MAIN, 172);
			player->ADD_GOSSIP_ITEM(1, "'50 Agility & 25 Mastery'", GOSSIP_SENDER_MAIN, 173);
            player->ADD_GOSSIP_ITEM(1, "<-Return", GOSSIP_SENDER_MAIN, 300);
            player->PlayerTalkClass->SendGossipMenu(100006, creature->GetGUID());
            return true;
            break;
 
        case 6: // Enchant Cloak
			player->ADD_GOSSIP_ITEM(1, "'22 Agility'", GOSSIP_SENDER_MAIN, 152);
			player->ADD_GOSSIP_ITEM(1, "'50 Intellect'", GOSSIP_SENDER_MAIN, 119);
            player->ADD_GOSSIP_ITEM(1, "'65 Critical Strike'", GOSSIP_SENDER_MAIN, 118);
            player->ADD_GOSSIP_ITEM(1, "'70 Spell Penetration'", GOSSIP_SENDER_MAIN, 120);
            player->ADD_GOSSIP_ITEM(1, "'250 Armor'", GOSSIP_SENDER_MAIN, 121);
            player->ADD_GOSSIP_ITEM(1, "<-Return", GOSSIP_SENDER_MAIN, 300);
            player->PlayerTalkClass->SendGossipMenu(100007, creature->GetGUID());
            return true;
            break;
 
        case 7: //Enchant Chest
			player->ADD_GOSSIP_ITEM(1, "'40 Resilience Rating'", GOSSIP_SENDER_MAIN, 124);
			player->ADD_GOSSIP_ITEM(1, "'20 of all stats'", GOSSIP_SENDER_MAIN, 125);
            player->ADD_GOSSIP_ITEM(1, "'40 Spirit'", GOSSIP_SENDER_MAIN, 122);
            player->ADD_GOSSIP_ITEM(1, "'75 Stamina'", GOSSIP_SENDER_MAIN, 123);
            player->ADD_GOSSIP_ITEM(1, "<-Return", GOSSIP_SENDER_MAIN, 300);
            player->PlayerTalkClass->SendGossipMenu(100008, creature->GetGUID());
            return true;
            break;
 
        case 8: //Enchant Bracers
            player->ADD_GOSSIP_ITEM(1, "'50 Agility'", GOSSIP_SENDER_MAIN, 126);
			player->ADD_GOSSIP_ITEM(1, "'50 Strength'", GOSSIP_SENDER_MAIN, 131);
			player->ADD_GOSSIP_ITEM(1, "'50 Intellect'", GOSSIP_SENDER_MAIN, 132);
            player->ADD_GOSSIP_ITEM(1, "'50 Spirit'", GOSSIP_SENDER_MAIN, 127);
			player->ADD_GOSSIP_ITEM(1, "'65 Haste Rating'", GOSSIP_SENDER_MAIN, 130);
            player->ADD_GOSSIP_ITEM(1, "'65 Critical Strike'", GOSSIP_SENDER_MAIN, 128);
			player->ADD_GOSSIP_ITEM(1, "'50 Hit Rating'", GOSSIP_SENDER_MAIN, 133);
            player->ADD_GOSSIP_ITEM(1, "'50 Expertise Rating'", GOSSIP_SENDER_MAIN, 129);
            player->ADD_GOSSIP_ITEM(1, "'50 Dodge Rating'", GOSSIP_SENDER_MAIN, 134);
            player->ADD_GOSSIP_ITEM(1, "<-Return", GOSSIP_SENDER_MAIN, 300);
            player->PlayerTalkClass->SendGossipMenu(100009, creature->GetGUID());
            return true;
            break;
 
        case 9: //Enchant Gloves
			player->ADD_GOSSIP_ITEM(1, "'20 Agility'", GOSSIP_SENDER_MAIN, 153);
			player->ADD_GOSSIP_ITEM(1, "'50 Strength'", GOSSIP_SENDER_MAIN, 138);
			player->ADD_GOSSIP_ITEM(1, "'50 Haste Rating'", GOSSIP_SENDER_MAIN, 137);
            player->ADD_GOSSIP_ITEM(1, "'65 Mastery Rating'", GOSSIP_SENDER_MAIN, 136);
			player->ADD_GOSSIP_ITEM(1, "'50 Expertise Rating'", GOSSIP_SENDER_MAIN, 135);
            player->ADD_GOSSIP_ITEM(1, "<-Return", GOSSIP_SENDER_MAIN, 300);
            player->PlayerTalkClass->SendGossipMenu(100010, creature->GetGUID());
            return true;
            break;

		case 13: //Enchant Belt
			player->ADD_GOSSIP_ITEM(1, "'+1 Gem Socket'", GOSSIP_SENDER_MAIN, 154);
			player->ADD_GOSSIP_ITEM(1, "<-Return", GOSSIP_SENDER_MAIN, 300);
			player->PlayerTalkClass->SendGossipMenu(100010, creature->GetGUID());
			return true;
			break;
 
        case 10: //Enchant Feet
			player->ADD_GOSSIP_ITEM(1, "'35 Agility'", GOSSIP_SENDER_MAIN, 142);
			player->ADD_GOSSIP_ITEM(1, "'50 Haste Rating'", GOSSIP_SENDER_MAIN, 140);
			player->ADD_GOSSIP_ITEM(1, "'50 Mastery Rating'", GOSSIP_SENDER_MAIN, 143);
			player->ADD_GOSSIP_ITEM(1, "'50 Hit Rating'", GOSSIP_SENDER_MAIN, 144);
            player->ADD_GOSSIP_ITEM(1, "'25 Agility & +Mov. Speed'", GOSSIP_SENDER_MAIN, 139);
			player->ADD_GOSSIP_ITEM(1, "'35 Mastery Rating & +Mov. Speed'", GOSSIP_SENDER_MAIN, 141);
            player->ADD_GOSSIP_ITEM(1, "<-Return", GOSSIP_SENDER_MAIN, 300);
            player->PlayerTalkClass->SendGossipMenu(100010, creature->GetGUID());
            return true;
            break;
 
        case 11: //Enchant Legs
			player->ADD_GOSSIP_ITEM(1, "'190 Attack Power & 55 Crit. Strike'", GOSSIP_SENDER_MAIN, 161);
            player->ADD_GOSSIP_ITEM(1, "'145 Stamina & 55 Agility'", GOSSIP_SENDER_MAIN, 160);
            player->ADD_GOSSIP_ITEM(1, "'95 Intellect & 80 Stamina'", GOSSIP_SENDER_MAIN, 162);
            player->ADD_GOSSIP_ITEM(1, "'95 Intellect & 55 Spirit'", GOSSIP_SENDER_MAIN, 163);
            player->ADD_GOSSIP_ITEM(1, "'145 Stamina & 55 Dodge'", GOSSIP_SENDER_MAIN, 164);
            player->ADD_GOSSIP_ITEM(1, "<-Return", GOSSIP_SENDER_MAIN, 300);
            player->PlayerTalkClass->SendGossipMenu(100010, creature->GetGUID());
            return true;
            break;
 
        case 12: //Enchant rings
			player->ADD_GOSSIP_ITEM(1, "'40 Strength'", GOSSIP_SENDER_MAIN, 148);
            player->ADD_GOSSIP_ITEM(1, "'40 Agility'", GOSSIP_SENDER_MAIN, 145);
			player->ADD_GOSSIP_ITEM(1, "'40 Intellect'", GOSSIP_SENDER_MAIN, 147);
            player->ADD_GOSSIP_ITEM(1, "'60 Stamina'", GOSSIP_SENDER_MAIN, 146);
            player->ADD_GOSSIP_ITEM(1, "<-Return", GOSSIP_SENDER_MAIN, 300);
            player->PlayerTalkClass->SendGossipMenu(100013, creature->GetGUID());
            return true;
            break;
 
        case 100:
            if (player->GetMoney() < ONE_HAND_WEAPON_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", ONE_HAND_WEAPON_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-ONE_HAND_WEAPON_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_MONGOOSE);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 101:
            if (player->GetMoney() < ONE_HAND_WEAPON_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", ONE_HAND_WEAPON_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-ONE_HAND_WEAPON_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_ELEMENTAL_SLAYER);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 102:
            if (player->GetMoney() < ONE_HAND_WEAPON_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", ONE_HAND_WEAPON_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-ONE_HAND_WEAPON_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_HEARTSONG);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 103:
            if (player->GetMoney() < ONE_HAND_WEAPON_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", ONE_HAND_WEAPON_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-ONE_HAND_WEAPON_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_HURRICANE);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 104:
            if (player->GetMoney() < ONE_HAND_WEAPON_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", ONE_HAND_WEAPON_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-ONE_HAND_WEAPON_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_LANDSLIDE);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        /*case 105:
            if (player->GetMoney() < ONE_HAND_WEAPON_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", ONE_HAND_WEAPON_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-ONE_HAND_WEAPON_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_MENDING);
            player->CLOSE_GOSSIP_MENU();
            break;*/
 
        case 106:
            if (player->GetMoney() < ONE_HAND_WEAPON_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", ONE_HAND_WEAPON_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-ONE_HAND_WEAPON_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_POWER_TORRENT);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 107:
            if (player->GetMoney() < ONE_HAND_WEAPON_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", ONE_HAND_WEAPON_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-ONE_HAND_WEAPON_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_WINDWALK);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 217:
            if (player->GetMoney() < ONE_HAND_WEAPON_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", ONE_HAND_WEAPON_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-ONE_HAND_WEAPON_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_PYRIUM_WEAPON_CHAIN);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 108:
            if (player->GetMoney() < TWO_HAND_WEAPON_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", TWO_HAND_WEAPON_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            mainItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);
            if (mainItem->GetTemplate()->InventoryType == INVTYPE_2HWEAPON)
            {
                player->ModifyMoney(-TWO_HAND_WEAPON_COST);
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_2HWEP_MIGHTY_AGILITY);
                player->CLOSE_GOSSIP_MENU();
            }
            else
            {
                player->GetSession()->SendAreaTriggerMessage("|cffFF0000You don't have 2H equipped!");
                player->CLOSE_GOSSIP_MENU();
            }
            break;
 
        case 109:
            if (player->GetMoney() < SHIELD_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", SHIELD_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            mainItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);
            if (mainItem->GetTemplate()->InventoryType == INVTYPE_SHIELD)
            {
                player->ModifyMoney(-SHIELD_COST);
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_SHIELD_MASTERY);
                player->CLOSE_GOSSIP_MENU();
            }
            else
            {
                player->GetSession()->SendAreaTriggerMessage("|cffFF0000You don't have Shield equipped!");
                player->CLOSE_GOSSIP_MENU();
            }
            break;
 
        case 110:
            if (player->GetMoney() < SHIELD_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", SHIELD_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            mainItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);
            if (mainItem->GetTemplate()->InventoryType == INVTYPE_SHIELD)
            {
                player->ModifyMoney(-SHIELD_COST);
                Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_SHIELD_PROTECTION);
                player->CLOSE_GOSSIP_MENU();
            }
            else
            {
                player->GetSession()->SendAreaTriggerMessage("|cffFF0000You don't have Shield equipped!");
                player->CLOSE_GOSSIP_MENU();
            }
            break;
 
        case 111:
            if (player->GetMoney() < SHIELD_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", SHIELD_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-SHIELD_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_OFF_HAND_SUPERIOR_INTELLECT);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 112:
            if (player->GetMoney() < MAIN_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-MAIN_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_VICIOUS_AGILITY);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 113:
            if (player->GetMoney() < MAIN_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-MAIN_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_VICIOUS_INTELLECT);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 114:
            if (player->GetMoney() < MAIN_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-MAIN_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_VICIOUS_STRENGTH);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 115:
            if (player->GetMoney() < MAIN_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-MAIN_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_VICIOUS_AGILITY);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 116:
            if (player->GetMoney() < MAIN_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-MAIN_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_VICIOUS_INTELLECT);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 117:
            if (player->GetMoney() < MAIN_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-MAIN_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_VICIOUS_STRENGTH);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 118:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", OFF_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_GREATER_CRITICAL_STRIKE);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 119:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", OFF_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_GREATER_INTELLECT);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 120:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", OFF_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_GREATER_SPELL_PIERCING);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 121:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", OFF_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_PROTECTION);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 122:
            if (player->GetMoney() < MAIN_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-MAIN_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST), ENCHANT_CHEST_EXCEPTIONAL_SPIRIT);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 123:
            if (player->GetMoney() < MAIN_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-MAIN_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST), ENCHANT_CHEST_GREATER_STAMINA);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 124:
            if (player->GetMoney() < MAIN_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-MAIN_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST), ENCHANT_CHEST_MIGHTY_RESILIENCE);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 125:
            if (player->GetMoney() < MAIN_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-MAIN_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST), ENCHANT_CHEST_PEERLESS_STATS);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 126:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", OFF_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_AGILITY);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 127:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", OFF_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_EXCEPTIONAL_SPIRIT);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 128:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", OFF_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_GREATER_CRITICAL_STRIKE);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 129:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", OFF_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_GREATER_EXPERTISE);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 130:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", OFF_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_GREATER_SPEED);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 131:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", OFF_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_MAJOR_STRENGTH);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 132:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", OFF_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_MIGHTY_INTELLECT);
            player->CLOSE_GOSSIP_MENU();
            break;
        case 133:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", OFF_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_PRECISION);
            player->CLOSE_GOSSIP_MENU();
            break;
        case 134:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", OFF_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_BRACERS_SUPERIOR_DODGE);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 135:
            if (player->GetMoney() < MAIN_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-MAIN_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_GREATER_EXPERTISE);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 136:
            if (player->GetMoney() < MAIN_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-MAIN_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_GREATER_MASTERY);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 137:
            if (player->GetMoney() < MAIN_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-MAIN_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_HASTE);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 138:
            if (player->GetMoney() < MAIN_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-MAIN_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_MIGHTY_STRENGTH);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 139:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_ASSASSINS_STEP);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 140:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_HASTE);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 141:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_LAVAWALKER);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 142:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_MAJOR_AGILITY);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 143:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_MASTERY);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 144:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_PRECISION);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 160:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS), ENCHANT_LEGS_CHARSCALE_LEG_ARMOR);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 161:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS), ENCHANT_LEGS_DRAGONSCALE_LEG_ARMOR);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 162:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS), ENCHANT_LEGS_POWERFUL_ENCHANTED_SPELLTHREAD);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 163:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS), ENCHANT_LEGS_POWERFUL_GHOSTLY_SPELLTHREAD);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 164:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS), ENCHANT_DRAKEHIDE_LEG_ARMOR);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 145:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER1), ENCHANT_RING_AGILITY);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER2), ENCHANT_RING_AGILITY);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 146:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER1), ENCHANT_RING_GREATER_STAMINA);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER2), ENCHANT_RING_GREATER_STAMINA);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 147:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
        case 150:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
        case 151:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER1), ENCHANT_RING_INTELLECT);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER2), ENCHANT_RING_INTELLECT);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 148:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }
 
            player->ModifyMoney(-OFF_PIECES_COST);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER1), ENCHANT_RING_STRENGTH);
            Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER2), ENCHANT_RING_STRENGTH);
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 149:
            if (player->GetMoney() < OFF_PIECES_COST) {
                ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
                player->CLOSE_GOSSIP_MENU();;
                break;
            }

		case 152:
			if (player->GetMoney() < OFF_PIECES_COST) {
				ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", OFF_PIECES_COST);
				player->CLOSE_GOSSIP_MENU();;
				break;
			}

			player->ModifyMoney(-OFF_PIECES_COST);
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_MAJOR_AGILITY);
			player->CLOSE_GOSSIP_MENU();
			break;

		case 153:
			if (player->GetMoney() < OFF_PIECES_COST) {
				ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", OFF_PIECES_COST);
				player->CLOSE_GOSSIP_MENU();;
				break;
			}

			player->ModifyMoney(-OFF_PIECES_COST);
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_AGILITY);
			player->CLOSE_GOSSIP_MENU();
			break;

		case 154: // Socket Belt
			AdvancedEnchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WAIST), PRISMATIC_ENCHANTMENT_SLOT, ENCHANT_BELT_SOCKET); 
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 155:
			if (player->GetMoney() < MAIN_PIECES_COST) {
				ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
				player->CLOSE_GOSSIP_MENU();;
				break;
			}

			player->ModifyMoney(-MAIN_PIECES_COST);
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_UNBREAKABLE_QUARTZ);
			player->CLOSE_GOSSIP_MENU();
			break;

		case 156:
			if (player->GetMoney() < MAIN_PIECES_COST) {
				ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
				player->CLOSE_GOSSIP_MENU();;
				break;
			}

			player->ModifyMoney(-MAIN_PIECES_COST);
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_CHARGED_LODESTONE);
			player->CLOSE_GOSSIP_MENU();
			break;

		case 157:
			if (player->GetMoney() < MAIN_PIECES_COST) {
				ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
				player->CLOSE_GOSSIP_MENU();;
				break;
			}

			player->ModifyMoney(-MAIN_PIECES_COST);
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_JAGGED_STONE);
			player->CLOSE_GOSSIP_MENU();
			break;

		case 158:
			if (player->GetMoney() < MAIN_PIECES_COST) {
				ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
				player->CLOSE_GOSSIP_MENU();;
				break;
			}

			player->ModifyMoney(-MAIN_PIECES_COST);
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD), ENCHANT_HEAD_SHATTERED_CRYSTAL);
			player->CLOSE_GOSSIP_MENU();
			break;
		case 170:
			if (player->GetMoney() < MAIN_PIECES_COST) {
				ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
				player->CLOSE_GOSSIP_MENU();;
				break;
			}

			player->ModifyMoney(-MAIN_PIECES_COST);
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_UNBREAKABLE_QUARTZ);
			player->CLOSE_GOSSIP_MENU();
			break;
		case 171:
			if (player->GetMoney() < MAIN_PIECES_COST) {
				ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
				player->CLOSE_GOSSIP_MENU();;
				break;
			}

			player->ModifyMoney(-MAIN_PIECES_COST);
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_CHARGED_LODESTONE);
			player->CLOSE_GOSSIP_MENU();
			break;
		case 172:
			if (player->GetMoney() < MAIN_PIECES_COST) {
				ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
				player->CLOSE_GOSSIP_MENU();;
				break;
			}

			player->ModifyMoney(-MAIN_PIECES_COST);
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_JAGGED_STONE);
			player->CLOSE_GOSSIP_MENU();
			break;
		case 173:
			if (player->GetMoney() < MAIN_PIECES_COST) {
				ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
				player->CLOSE_GOSSIP_MENU();;
				break;
			}

			player->ModifyMoney(-MAIN_PIECES_COST);
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SHOULDER_SHATTERED_CRYSTAL);
			player->CLOSE_GOSSIP_MENU();
			break;
		case 174:
			if (player->GetMoney() < MAIN_PIECES_COST) {
				ChatHandler(player->GetSession()).PSendSysMessage("You don't have enough money to purchase this! It costs %d gold", MAIN_PIECES_COST);
				player->CLOSE_GOSSIP_MENU();;
				break;
			}

			player->ModifyMoney(-MAIN_PIECES_COST);
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_RANGED), ENCHANT_RANGED_AGILITY);
			player->CLOSE_GOSSIP_MENU();
			break;
 
        case 300:
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_axe_1h_deathwingraiddw_d_01:25|t Enchant Mainhand Weapon", GOSSIP_SENDER_MAIN, 1);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_axe_1h_deathwingraiddw_d_01:25|t Enchant Offhand Weapon & Shield", GOSSIP_SENDER_MAIN, 3);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_axe_68:25|t Enchant Two Hands Weapon", GOSSIP_SENDER_MAIN, 2);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_helmet_78:25|t Enchant Helmet", GOSSIP_SENDER_MAIN, 4);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_shoulder_90:25|t Enchant Shoulders", GOSSIP_SENDER_MAIN, 5);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_misc_cape_22:25|t Enchant Cloak", GOSSIP_SENDER_MAIN, 6);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_chest_plate23:25|t Enchant Armor", GOSSIP_SENDER_MAIN, 7);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_bracer_25b:25|t Enchant Bracers", GOSSIP_SENDER_MAIN, 8);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_gauntlets_robe_raidpriest_j_01:25|t Enchant Gloves", GOSSIP_SENDER_MAIN, 9);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_belt_02:25|t Enchant Belt", GOSSIP_SENDER_MAIN, 13);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_pants_robe_raidpriest_j_01:25|t Enchant Pants", GOSSIP_SENDER_MAIN, 11);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_boots_cloth_16:25|t Enchant Boots", GOSSIP_SENDER_MAIN, 10);
 
            if (player->HasSkill(SKILL_ENCHANTING) && player->GetSkillValue(SKILL_ENCHANTING) == 450)
                player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_jewelry_ring_78:25|t Enchant Rings", GOSSIP_SENDER_MAIN, 12);
 
            player->PlayerTalkClass->SendGossipMenu(100001, creature->GetGUID());
            break;
        }
        return true;
    } 
};
 
void AddSC_npc_enchantment()
{
    new npc_enchantment();
}