﻿//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"

class Learn_Talent_Reset : public CreatureScript
{
public:
	Learn_Talent_Reset() : CreatureScript("Learn_Talent_Reset") {}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|cff00ff00|TInterface\\icons\\Misc_arrowright:40|t |rLearn Dual Specialization", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|cff00ff00|TInterface\\icons\\Misc_arrowright:40|t |rReset my talents", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
		if (player->getClass() == CLASS_HUNTER)
		{
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|cff00ff00|TInterface\\icons\\Misc_arrowright:40|t |rReset my pet talents", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3);
		}
		player->PlayerTalkClass->SendGossipMenu(907, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 /*uiSender*/, uint32 action)
	{
		if (!player || !creature)
			return true;

		player->PlayerTalkClass->ClearMenus();

		switch (action)
		{
		case GOSSIP_ACTION_INFO_DEF + 1:
		{
			// learn dual spec
			player->CastSpell(player, 63680, true, NULL, NULL, player->GetGUID());
			player->CastSpell(player, 63624, true, NULL, NULL, player->GetGUID());

			ChatHandler(player->GetSession()).PSendSysMessage("You've learnt dual specialization!");
			player->PlayerTalkClass->SendCloseGossip();
		}
		break;
		case GOSSIP_ACTION_INFO_DEF + 2:
		{
			player->ResetTalents(true);
			player->SendTalentsInfoData(false);
			ChatHandler(player->GetSession()).PSendSysMessage("Succesfully reset talents!");
			player->PlayerTalkClass->SendCloseGossip();
		}
		break;
		case GOSSIP_ACTION_INFO_DEF + 3:
		{
			player->ResetPetTalents();
			player->SendTalentsInfoData(false);
			ChatHandler(player->GetSession()).PSendSysMessage("Succesfully reset pet talents!");
			player->PlayerTalkClass->SendCloseGossip();
		}
		break;
		default:
			break;
		}
		return true;
	}
};

void AddSC_Learn_Talent_Reset()
{
	new Learn_Talent_Reset();
}
