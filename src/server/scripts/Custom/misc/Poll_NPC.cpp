﻿//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"

class Poll_NPC : public CreatureScript
{
public:
	Poll_NPC() : CreatureScript("Poll_NPC") {}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "Enter feedback", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1, "", 0, true);
		//player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "What is this?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 190001);
		ViewGossipItems1(player);
		player->PlayerTalkClass->SendGossipMenu(907, creature->GetGUID());
		return true;
	}

	void DeveloperResponse(Player* player)
	{
		QueryResult howmanyreports = CharacterDatabase.PQuery("SELECT devcomment FROM feedback_player WHERE guid = '%u'", player->GetGUIDLow());
		uint32 list = 0;

		if (howmanyreports)
		{
			Field* fields = howmanyreports->Fetch();
			char const* devcomments = fields[0].GetCString();
			ChatHandler(player->GetSession()).SendSysMessage("Developer response:");
			do
			{
				ChatHandler(player->GetSession()).PSendSysMessage("========================");
				ChatHandler(player->GetSession()).PSendSysMessage("Report %u: %s ", ++list, devcomments);
			} while (howmanyreports->NextRow());
			ChatHandler(player->GetSession()).PSendSysMessage("========================");
		}
		else
			return;
	}

	void InsertingPreparedStatment(Player* player, std::string& feedback, uint32 report)
	{
		PreparedStatement * stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_FEEDBACK_PLAYER);
		stmt->setUInt32(0, player->GetGUIDLow());
		stmt->setString(1, player->GetName().c_str());
		stmt->setString(2, feedback);
		stmt->setUInt32(3, report);
		CharacterDatabase.Execute(stmt);
		player->GetSession()->SendAreaTriggerMessage("Thank you for your feedback!");
	}
	
	// cant be assed to put it into one void function, but this is fine
	void Viewdeletefeedback1(Player* player)
	{
		QueryResult howmanyreports = CharacterDatabase.PQuery("SELECT reports FROM feedback_player WHERE reports = 1 AND guid = '%u'", player->GetGUIDLow());

		if (howmanyreports)
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Delete report 1", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 5);
	}

	void Viewdeletefeedback2(Player* player)
	{
		QueryResult howmanyreports = CharacterDatabase.PQuery("SELECT reports FROM feedback_player WHERE reports = 2 AND guid = '%u'", player->GetGUIDLow());

		if (howmanyreports)
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Delete report 2", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 6);
	}
	void Viewdeletefeedback3(Player* player)
	{
		QueryResult howmanyreports = CharacterDatabase.PQuery("SELECT reports FROM feedback_player WHERE reports = 3 AND guid = '%u'", player->GetGUIDLow());

		if (howmanyreports)
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Delete report 3", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 7);
	}
	void Viewdeletefeedback4(Player* player)
	{
		QueryResult howmanyreports = CharacterDatabase.PQuery("SELECT reports FROM feedback_player WHERE reports = 4 AND guid = '%u'", player->GetGUIDLow());

		if (howmanyreports)
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Delete report 4", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 8);
	}
	void Viewdeletefeedback5(Player* player)
	{
		QueryResult howmanyreports = CharacterDatabase.PQuery("SELECT reports FROM feedback_player WHERE reports = 5 AND guid = '%u'", player->GetGUIDLow());

		if (howmanyreports)
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Delete report 5", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 9);
	}

	void DeleteFeedback(Player* player, uint32& report)
	{
		CharacterDatabase.PExecute("DELETE FROM feedback_player WHERE reports = '%u' AND guid = '%u'", report, player->GetGUIDLow());
	}

	void ViewGossipItems1(Player* player)
	{
		QueryResult viewreport = CharacterDatabase.PQuery("SELECT comment FROM feedback_player WHERE guid = '%u'", player->GetGUIDLow());

		if (viewreport)
		{
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "View my reports", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2); // display should player have a report?
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Delete reports", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "View Developer comments", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1337);
		}
		else
			return;
	}

	void ViewFeedback(Player* player)
	{
		QueryResult viewreport = CharacterDatabase.PQuery("SELECT comment FROM feedback_player WHERE guid = '%u'", player->GetGUIDLow());
		uint32 list = 0;

		if (viewreport)
		{
			ChatHandler(player->GetSession()).SendSysMessage("Your Reports:");
			do
			{
				Field* fields = viewreport->Fetch();
				char const* comments = fields[0].GetCString();
				ChatHandler(player->GetSession()).PSendSysMessage("========================");
				ChatHandler(player->GetSession()).PSendSysMessage("Report %u: %s ", ++list, comments);
			} while (viewreport->NextRow());
			ChatHandler(player->GetSession()).PSendSysMessage("========================");
		}
		else
			return; // should never really happen but just incase
	}

	void InsertFeedback2(Player* player, std::string& feedback)
	{
		QueryResult selectreport = CharacterDatabase.PQuery("SELECT COUNT(guid) FROM feedback_player WHERE guid = '%u'", player->GetGUIDLow());
		QueryResult reporting1 = CharacterDatabase.PQuery("SELECT reports FROM feedback_player WHERE reports = 1 AND guid = '%u'", player->GetGUIDLow());
		QueryResult reporting2 = CharacterDatabase.PQuery("SELECT reports FROM feedback_player WHERE reports = 2 AND guid = '%u'", player->GetGUIDLow());
		QueryResult reporting3 = CharacterDatabase.PQuery("SELECT reports FROM feedback_player WHERE reports = 3 AND guid = '%u'", player->GetGUIDLow());
		QueryResult reporting4 = CharacterDatabase.PQuery("SELECT reports FROM feedback_player WHERE reports = 4 AND guid = '%u'", player->GetGUIDLow());
		QueryResult reporting5 = CharacterDatabase.PQuery("SELECT reports FROM feedback_player WHERE reports = 5 AND guid = '%u'", player->GetGUIDLow());

		uint64 reportcount = !selectreport ? 0 : selectreport->Fetch()[0].GetUInt64();
		if (!reportcount || reportcount >= 1)
		{
			if (!reportcount)
			{
				InsertingPreparedStatment(player, feedback, 1);
				return;
			}

			if (reportcount == 1)
			{
				InsertingPreparedStatment(player, feedback, 2);
				return;
			}
			if (reportcount == 2)
			{
				if (!reporting1)
				{
					InsertingPreparedStatment(player, feedback, 1);
				}
				else if (!reporting2)
				{
					InsertingPreparedStatment(player, feedback, 2);
				}
				else if (!reporting3)
				{
					InsertingPreparedStatment(player, feedback, 3);
				}
				else if (!reporting4)
				{
					InsertingPreparedStatment(player, feedback, 4);
				}
				else if (!reporting5)
				{
					InsertingPreparedStatment(player, feedback, 5);
				}
				else
				{
					InsertingPreparedStatment(player, feedback, 3);
				}
			}
			if (reportcount == 3)
			{
				if (!reporting1)
				{
					InsertingPreparedStatment(player, feedback, 1);
				}
				else if (!reporting2)
				{
					InsertingPreparedStatment(player, feedback, 2);
				}
				else if (!reporting3)
				{
					InsertingPreparedStatment(player, feedback, 3);
				}
				else if (!reporting4)
				{
					InsertingPreparedStatment(player, feedback, 4);
				}
				else if (!reporting5)
				{
					InsertingPreparedStatment(player, feedback, 5);
				}
				else
				{
					InsertingPreparedStatment(player, feedback, 4);
				}
			}
			if (reportcount == 4)
			{
				if (!reporting1)
				{
					InsertingPreparedStatment(player, feedback, 1);
				}
				else if (!reporting2)
				{
					InsertingPreparedStatment(player, feedback, 2);
				}
				else if (!reporting3)
				{
					InsertingPreparedStatment(player, feedback, 3);
				}
				else if (!reporting4)
				{
					InsertingPreparedStatment(player, feedback, 4);
				}
				else if (!reporting5)
				{
					InsertingPreparedStatment(player, feedback, 5);
				}
				else
				{
					InsertingPreparedStatment(player, feedback, 5);
				}
			}
			if (reportcount >= 5)
				player->GetSession()->SendAreaTriggerMessage("You've reached your bug report limit!");
			return;
		}
	}


	bool OnGossipSelect(Player *player, Creature * creature, uint32 sender, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();
		
		uint32 report;
		switch (action)
		{
		case GOSSIP_ACTION_INFO_DEF + 2:
			ViewFeedback(player);
			player->PlayerTalkClass->SendCloseGossip();
			break;
		case GOSSIP_ACTION_INFO_DEF + 3:
			player->PlayerTalkClass->ClearMenus();
			Viewdeletefeedback1(player);
			Viewdeletefeedback2(player);
			Viewdeletefeedback3(player);
			Viewdeletefeedback4(player);
			Viewdeletefeedback5(player);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Previous Page", GOSSIP_SENDER_MAIN, 15);
			player->SEND_GOSSIP_MENU(60020, creature->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 5:
			report = 1;
			DeleteFeedback(player, report);
			player->PlayerTalkClass->SendCloseGossip();
			player->GetSession()->SendAreaTriggerMessage("Deleted report 1!");
			break;
		case GOSSIP_ACTION_INFO_DEF + 6:
			report = 2;
			DeleteFeedback(player, report);
			player->PlayerTalkClass->SendCloseGossip();
			player->GetSession()->SendAreaTriggerMessage("Deleted report 2!");
			break;
		case GOSSIP_ACTION_INFO_DEF + 7:
			report = 3;
			DeleteFeedback(player, report);
			player->PlayerTalkClass->SendCloseGossip();
			player->GetSession()->SendAreaTriggerMessage("Deleted report 3!");
			break;
		case GOSSIP_ACTION_INFO_DEF + 8:
			report = 4;
			DeleteFeedback(player, report);
			player->PlayerTalkClass->SendCloseGossip();
			player->GetSession()->SendAreaTriggerMessage("Deleted report 4!");
			break;
		case GOSSIP_ACTION_INFO_DEF + 9:
			report = 5;
			DeleteFeedback(player, report);
			player->PlayerTalkClass->SendCloseGossip();
			player->GetSession()->SendAreaTriggerMessage("Deleted report 5!");
			break;
		case GOSSIP_ACTION_INFO_DEF + 1337:
			DeveloperResponse(player);
			player->PlayerTalkClass->SendCloseGossip();
			break;
		case 15:
			OnGossipHello(player, creature);
			break;
		case GOSSIP_ACTION_INFO_DEF + 190001:
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "This is a feedback NPC. Allows players to write feedback regarding any bug report or suggestions which a developer can leave a response for you to read.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 10000);
		default:
			break;
		}
		return true;
	}

	bool OnGossipSelectCode(Player* player, Creature* creature, uint32 sender, uint32 action, char const* code)
	{
		player->PlayerTalkClass->ClearMenus();
		if (sender == GOSSIP_SENDER_MAIN)
		{
			switch (action)
			{
			case GOSSIP_ACTION_INFO_DEF + 1:
			{
				if (strlen(code) < 10)
				{
					player->GetSession()->SendAreaTriggerMessage("You need to type more than 10 characters to submit a report!");
					player->PlayerTalkClass->SendCloseGossip();
					return false;
				}
				std::string feedback;
				feedback = code;
				InsertFeedback2(player, feedback);
				player->PlayerTalkClass->SendCloseGossip();
				break;
			}
			}
		}

		return true;
	}
};

void AddSC_Poll_NPC()
{
	new Poll_NPC();
}