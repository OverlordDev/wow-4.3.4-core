﻿//Syndicate-WoW | Cataclysm

#include "Language.h"
#include "Player.h"
#include "Define.h"
#include "Creature.h"
#include "ScriptMgr.h"
#include "WorldSession.h"
#include "ScriptedGossip.h"
#include "Chat.h"

class Professions_NPC : public CreatureScript
{
public:
	Professions_NPC() : CreatureScript("Professions_NPC") { }

	bool OnGossipHello(Player *player, Creature* creature)
	{
		player->ADD_GOSSIP_ITEM(11, "|TInterface/ICONS/Inv_crown_15:40|t |CFF190707 Primary Professions", GOSSIP_SENDER_MAIN, 1);
		player->ADD_GOSSIP_ITEM(11, "|TInterface/ICONS/Inv_crown_14:40|t |CFF190707 Secondary Professions", GOSSIP_SENDER_MAIN, 2);
		player->PlayerTalkClass->SendGossipMenu(60030, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 uiSender, uint32 SKILL)
	{
		player->PlayerTalkClass->ClearMenus();

		if (uiSender == GOSSIP_SENDER_MAIN)
		{
			switch (SKILL)
			{
			case 1: // Primary Proffs
				player->ADD_GOSSIP_ITEM(3, "|TInterface/ICONS/Inv_misc_wrench_02:40|t|CFF190707 Engineering", GOSSIP_SENDER_MAIN, SKILL_ENGINEERING);
				player->ADD_GOSSIP_ITEM(3, "|TInterface/ICONS/Trade_blacksmithing:40|t|CFF190707 Blacksmithing", GOSSIP_SENDER_MAIN, SKILL_BLACKSMITHING);
				player->ADD_GOSSIP_ITEM(3, "|TInterface/ICONS/Trade_tailoring:40|t|CFF190707 Tailoring", GOSSIP_SENDER_MAIN, SKILL_TAILORING);
				player->ADD_GOSSIP_ITEM(3, "|TInterface/ICONS/Inv_inscription_tradeskill01:40|t|CFF190707 Inscription", GOSSIP_SENDER_MAIN, SKILL_INSCRIPTION);
				player->ADD_GOSSIP_ITEM(3, "|TInterface/ICONS/Inv_jewelcrafting_dragonseye03:40|t|CFF190707 Jewelcrafting", GOSSIP_SENDER_MAIN, SKILL_JEWELCRAFTING);
				player->ADD_GOSSIP_ITEM(3, "|TInterface/ICONS/Trade_engraving:40|t|CFF190707 Enchanting", GOSSIP_SENDER_MAIN, SKILL_ENCHANTING);
				player->ADD_GOSSIP_ITEM(3, "|TInterface/ICONS/Trade_herbalism:40|t|CFF190707 Herbalism", GOSSIP_SENDER_MAIN, SKILL_HERBALISM);
				player->ADD_GOSSIP_ITEM(3, "|TInterface/ICONS/Trade_alchemy:40|t|CFF190707 Alchemy", GOSSIP_SENDER_MAIN, SKILL_ALCHEMY);
				player->ADD_GOSSIP_ITEM(3, "|TInterface/ICONS/Inv_misc_pelt_bear_ruin_02:40|t|CFF190707 Leatherworking", GOSSIP_SENDER_MAIN, SKILL_LEATHERWORKING);
				player->ADD_GOSSIP_ITEM(3, "|TInterface/ICONS/Trade_mining:40|t|CFF190707 Mining", GOSSIP_SENDER_MAIN, SKILL_MINING);
				player->ADD_GOSSIP_ITEM(3, "|TInterface/ICONS/Inv_misc_skinningknife:40|t|CFF190707 Skinning", GOSSIP_SENDER_MAIN, SKILL_SKINNING);

				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 5);
				player->PlayerTalkClass->SendGossipMenu(60030, creature->GetGUID());
				break;
			case 2: // Secondary Proffs
				player->ADD_GOSSIP_ITEM(3, "|TInterface/ICONS/Inv_thanksgiving_turkey:40|t|CFF190707 Cooking", GOSSIP_SENDER_MAIN, SKILL_COOKING);
				player->ADD_GOSSIP_ITEM(3, "|TInterface/ICONS/Inv_misc_potionsetb:40|t|CFF190707 First Aid", GOSSIP_SENDER_MAIN, SKILL_FIRST_AID);
				//player->ADD_GOSSIP_ITEM(3, "Fishing", GOSSIP_SENDER_MAIN, SKILL_FISHING);
				//player->ADD_GOSSIP_ITEM(3, "|TInterface/ICONS/Inv_misc_archaeology_vrykuldrinkinghorn:40|t|CFF190707Archaeology", GOSSIP_SENDER_MAIN, SKILL_ARCHAEOLOGY);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, 5);
				player->PlayerTalkClass->SendGossipMenu(60030, creature->GetGUID());
				break;
			case 5: // Main Page
				OnGossipHello(player, creature);
				break;
			default:
			{
				if (player->HasSkill(SKILL))
					player->GetSession()->SendNotification("You already have that skill");
				else
				{
					CompleteLearnProfession(player, creature, (SkillType)SKILL);
					player->PlayerTalkClass->SendCloseGossip();
				}
			}
			}
		}
		return true;
	}

	bool IsSecondarySkill(SkillType skill) const
	{
		return skill == SKILL_COOKING || skill == SKILL_FIRST_AID || skill == SKILL_FISHING || skill == SKILL_ARCHAEOLOGY;
	}

	void CompleteLearnProfession(Player *player, Creature *creature, SkillType skill)
	{
		if (PlayerAlreadyHasTwoProfessions(player) && !IsSecondarySkill(skill))
			creature->MonsterWhisper("You've already have two professions!!", player->GetGUID());
		else
			if (!LearnAllRecipesInProfession(player, skill))
				creature->MonsterWhisper("An internal error occurred!", player->GetGUID());
	}

	bool LearnAllRecipesInProfession(Player *player, SkillType skill)
	{
		ChatHandler handler(player->GetSession());
		std::string skill_name;

		SkillLineEntry const *SkillInfo = sSkillLineStore.LookupEntry(skill);
		skill_name, SkillInfo->name[handler.GetSessionDbcLocale()];

		if (!SkillInfo)
		{
			TC_LOG_DEBUG("misc", "Profession NPC: received non-valid skill ID (LearnAllRecipesInProfession)");
			return false;
		}

		LearnSkillRecipesHelper(player, SkillInfo->id);

		uint16 maxLevel = player->GetPureMaxSkillValue(SkillInfo->id);
		player->SetSkill(SkillInfo->id, player->GetSkillStep(SkillInfo->id), maxLevel, maxLevel);
		player->removeSpell(20222); // Goblin Engineering (engineering fix by Infamous)
		player->removeSpell(20219); // Gnomish Engineering (engineering fix by Infamous)
		player->removeSpell(105518); // Open Box (engineering fix by Infamous)
		player->removeSpell(28675); //Alchemy fix 1
		player->removeSpell(28672); //Alchemy fix 2
		handler.PSendSysMessage(LANG_COMMAND_LEARN_ALL_RECIPES, skill_name.c_str());
		return true;
	}

	bool PlayerAlreadyHasTwoProfessions(Player *player)
	{
		uint32 skillCount = 0;

		if (player->HasSkill(SKILL_MINING))
			skillCount++;
		if (player->HasSkill(SKILL_SKINNING))
			skillCount++;
		if (player->HasSkill(SKILL_HERBALISM))
			skillCount++;

		if (skillCount >= 2)
			return true;

		for (uint32 i = 1; i < sSkillLineStore.GetNumRows(); ++i)
		{
			SkillLineEntry const *SkillInfo = sSkillLineStore.LookupEntry(i);
			if (!SkillInfo)
				continue;

			if (SkillInfo->categoryId == SKILL_CATEGORY_SECONDARY)
				continue;

			if ((SkillInfo->categoryId != SKILL_CATEGORY_PROFESSION) || !SkillInfo->canLink)
				continue;

			const uint32 skillID = SkillInfo->id;
			if (player->HasSkill(skillID))
				skillCount++;

			if (skillCount >= 2)
				return true;
		}
		return false;
	}

	void LearnSkillRecipesHelper(Player *player, uint32 skill_id)
	{
		uint32 classmask = player->getClassMask();

		for (uint32 j = 0; j < sSkillLineAbilityStore.GetNumRows(); ++j)
		{
			SkillLineAbilityEntry const *skillLine = sSkillLineAbilityStore.LookupEntry(j);
			if (!skillLine)
				continue;

			// wrong skill
			if (skillLine->skillId != skill_id)
				continue;

			// not high rank
			if (skillLine->forward_spellid)
				continue;

			// skip racial skills
			if (skillLine->racemask != 0)
				continue;

			// skip wrong class skills
			if (skillLine->classmask && (skillLine->classmask & classmask) == 0)
				continue;

			player->learnSpell(skillLine->spellId, false);
		}
	}
};

void AddSC_Professions_NPC()
{
	new Professions_NPC();
}