//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"
#include "Chat.h"
#include "Player.h"
#include "Config.h"
#include "WorldSession.h"

#define MSG_COLOR_BLUEVIOLET "|cFF8A2BE2"
#define FACTION_SPECIFIC 0
const char* CLASS_ICON;
const char* RACE_ICON;

std::string GetNameLink(Player* player)
{
	std::string name = player->GetName();
	std::string color;

	switch (player->getRace())
	{
	case RACE_BLOODELF:
		if (player->getGender() == GENDER_MALE)
			RACE_ICON = "|TInterface/ICONS/Achievement_boss_pathaleonthecalculator:20|t";
		else
			RACE_ICON = "|TInterface/ICONS/Achievement_Character_Bloodelf_Female:20|t";
		break;
	case RACE_DRAENEI:
		if (player->getGender() == GENDER_FEMALE)
			RACE_ICON = "|TInterface/ICONS/Achievement_Character_Draenei_Female:20|t";
		else
			RACE_ICON = "|TInterface/ICONS/Achievement_boss_exarch_maladaar:20|t";
		break;
	case RACE_DWARF:
		if (player->getGender() == GENDER_FEMALE)
			RACE_ICON = "|TInterface/ICONS/Inv_misc_head_dwarf_02:20|t";
		else
			RACE_ICON = "|TInterface/ICONS/Achievement_leader_king_magni_bronzebeard:20|t";
		break;
	case RACE_GNOME:
		if (player->getGender() == GENDER_FEMALE)
			RACE_ICON = "|TInterface/ICONS/Achievement_Character_Gnome_Female:20|t";
		else
			RACE_ICON = "|TInterface/ICONS/Achievement_Character_Gnome_Male:20|t";
		break;
	case RACE_HUMAN:
		if (player->getGender() == GENDER_FEMALE)
			RACE_ICON = "|TInterface/ICONS/Achievement_boss_grandwidowfaerlina:20|t";
		else
			RACE_ICON = "|TInterface/ICONS/Achievement_boss_bazil_thredd:20|t";
		break;
	case RACE_NIGHTELF:
		if (player->getGender() == GENDER_FEMALE)
			RACE_ICON = "|TInterface/ICONS/Achievement_leader_tyrande_whisperwind:20|t";
		else
			RACE_ICON = "|TInterface/ICONS/Achievement_Character_Nightelf_Male:20|t";
		break;
	case RACE_ORC:
		if (player->getGender() == GENDER_FEMALE)
			RACE_ICON = "|TInterface/ICONS/Inv_misc_head_orc_02:20|t";
		else
			RACE_ICON = "|TInterface/ICONS/Achievement_character_orc_male:20|t";
		break;
	case RACE_TAUREN:
		if (player->getGender() == GENDER_FEMALE)
			RACE_ICON = "|TInterface/ICONS/Inv_misc_head_tauren_02:20|t";
		else
			RACE_ICON = "|TInterface/ICONS/Achievment_boss_blackhorn:20|t";
		break;
	case RACE_TROLL:
		if (player->getGender() == GENDER_FEMALE)
			RACE_ICON = "|TInterface/ICONS/Achievement_Character_Troll_Female:20|t";
		else
			RACE_ICON = "|TInterface/ICONS/Achievement_zulgurub_zanzil:20|t";
		break;
	case RACE_UNDEAD_PLAYER:
		if (player->getGender() == GENDER_FEMALE)
			RACE_ICON = "|TInterface/ICONS/Inv_misc_head_undead_02:20|t";
		else
			RACE_ICON = "|TInterface/ICONS/Inv_misc_head_undead_01:20|t";
		break;
	case RACE_GOBLIN:
		if (player->getGender() == GENDER_FEMALE)
			RACE_ICON = "|TInterface/ICONS/Inv_mask_07:20|t";
		else
			RACE_ICON = "|TInterface/ICONS/Achievement_goblinhead:20|t";
		break;
	case RACE_WORGEN:
		if (player->getGender() == GENDER_FEMALE)
			RACE_ICON = "|TInterface/ICONS/Inv_mask_09:20|t";
		else
			RACE_ICON = "|TInterface/ICONS/Achievement_worganhead:20|t";
		break;
	}

	switch (player->getClass())
	{
	case CLASS_DEATH_KNIGHT:
		color = "|cffC41F3B";
		break;
	case CLASS_DRUID:
		color = "|cffFF7D0A";
		break;
	case CLASS_HUNTER:
		color = "|cffABD473";
		break;
	case CLASS_MAGE:
		color = "|cff69CCF0";
		break;
	case CLASS_PALADIN:
		color = "|cffF58CBA";
		break;
	case CLASS_PRIEST:
		color = "|cffFFFFFF";
		break;
	case CLASS_ROGUE:
		color = "|cffFFF569";
		break;
	case CLASS_SHAMAN:
		color = "|cff0070DE";
		break;
	case CLASS_WARLOCK:
		color = "|cff9482C9";
		break;
	case CLASS_WARRIOR:
		color = "|cffC79C6E";
		break;
	}
	return "|Hplayer:" + name + "|h" + RACE_ICON + "|cfff2f2f2[" + color + name + "|cfff2f2f2]|h|r";
}

class cs_world_chat : public CommandScript
{
public:
    cs_world_chat() : CommandScript("cs_world_chat") {}
 
    std::vector<ChatCommand> GetCommands() const override
    {
        static std::vector<ChatCommand> WorldChatCommandTable =
        {
			{ "world", SEC_PLAYER, true, &HandleWorldChatCommand, "", },
			{ "w", SEC_PLAYER, true, &HandleWorldChatCommand, "", },
			{ "wo", SEC_PLAYER, true, &HandleWorldChatCommand, "", },
			{ "chat", SEC_PLAYER, true, &HandleWorldChatCommand, "", },
			{ "c", SEC_PLAYER, true, &HandleWorldChatCommand, "", },
		};

		return WorldChatCommandTable;
	}

	static bool HandleWorldChatCommand(ChatHandler * handler, const char * args)
	{
		if (sWorld->getIntConfig(CONFIG_WORLD_CHAT_ENABLED) == 0)
		{
			handler->PSendSysMessage("The world chat currently is disabled!");
			handler->SetSentErrorMessage(true);
			return false;
		}

		if (!handler->GetSession()->GetPlayer()->CanSpeak())
			return false;
		std::string temp = args;

		if (!args || temp.find_first_not_of(' ') == std::string::npos)
			return false;

		std::string msg = "";
		Player * player = handler->GetSession()->GetPlayer();

		switch (player->GetSession()->GetSecurity())
		{
		// Player
		case SEC_PLAYER:
			if (player->GetTeam() == ALLIANCE)
			{
				msg += "|cfff2f2f2[User]";
				msg += GetNameLink(player);
				msg += ":|cffadc2eb";
			}
			else
			{
				msg += "|cfff2f2f2[User]";
				msg += GetNameLink(player);
				msg += ": |cffffb399";
			}
			break;
		// VIP Player
		case SEC_VIP:
			if (player->GetTeam() == ALLIANCE)
			{
			msg += "|cffff8a00[VIP]";
			msg += GetNameLink(player);
			msg += ": |cfffaeb00";
			}
			else
			{
			msg += "|cffff8a00[VIP]";
			msg += GetNameLink(player);
			msg += ": |cfffaeb00";
			}
			break;
		// MVP Player
		case SEC_MVP:
			if (player->GetTeam() == ALLIANCE)
			{
			msg += "|cffff0080[MVP]";
			msg += GetNameLink(player);
			msg += ": |cfffaeb00";
			}
			else
			{
			msg += "|cffff0080[MVP]";
			msg += GetNameLink(player);
			msg += ": |cfffaeb00";
			}
			break;
		// Staff
		case SEC_MODERATOR:
			msg += "|cff00b300[TrialGM]";
			msg += " |TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:20|t|r ";
			msg += GetNameLink(player);
			msg += ": |cff99ff99";
			break;
		// Staff
		case SEC_JUNIOR_GM:
			msg += "|cff00b300[JuniorGM]";
			msg += " |TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:20|t|r ";
			msg += GetNameLink(player);
			msg += ": |cff99ff99";
			break;
		// Staff
		case SEC_GAMEMASTER:
			msg += "|cff00b300[GM]";
			msg += " |TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:20|t|r ";
			msg += GetNameLink(player);
			msg += ": |cff99ff99";
			break;
		// Staff
		case SEC_SENIOR_GM:
			msg += "|cff00b300[SeniorGM]";
			msg += " |TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:20|t|r ";
			msg += GetNameLink(player);
			msg += ": |cff99ff99";
			break;
		// Staff
		case SEC_DEVELOPER:
			msg += "|cff00b300[Dev]";
			msg += " |TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:20|t|r ";
			msg += GetNameLink(player);
			msg += ": |cff99ff99";
			break;
		// Staff
		case SEC_MANAGER:
			msg += "|cff00b300[Manager]";
			msg += " |TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:20|t|r ";
			msg += GetNameLink(player);
			msg += ": |cff99ff99";
			break;
		// Staff
		case SEC_ADMINISTRATOR:
			msg += "|cff00b300[Admin]";
			msg += " |TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:20|t|r ";
			msg += GetNameLink(player);
			msg += ": |cff99ff99";
			break;
		// Staff
		case SEC_OWNER:
			msg += "|cff00b300[Owner]";
			msg += " |TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:20|t|r ";
			msg += GetNameLink(player);
			msg += ": |cff99ff99";
			break;
		// Console
		case SEC_CONSOLE:
			msg += "|cff00b300[Console]";
			msg += " |TInterface/ICONS/Temp:20|t|r ";
			msg += GetNameLink(player);
			msg += ": |cff99ff99";
			break;
		}
		msg += args;
		if (FACTION_SPECIFIC)
		{
			SessionMap sessions = sWorld->GetAllSessions();
			for (SessionMap::iterator itr = sessions.begin(); itr != sessions.end(); ++itr)
			if (Player* plr = itr->second->GetPlayer())
			if (plr->GetTeam() == player->GetTeam())
				sWorld->SendServerMessage(SERVER_MSG_STRING, msg.c_str(), plr);
		}
		else
			sWorld->SendServerMessage(SERVER_MSG_STRING, msg.c_str(), 0);
		return true;
	}
};

void AddSC_cs_world_chat()
{
	new cs_world_chat();
}