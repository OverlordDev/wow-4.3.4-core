//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"

class Boss_Announcer : public PlayerScript
{
public:
	Boss_Announcer() : PlayerScript("Boss_Announcer") {}

	void OnCreatureKill(Player* player, Creature* boss)
	{

		if (boss->isWorldBoss())
		{
			if (player->getGender() == GENDER_MALE)
			{
				std::ostringstream ss;
				ss << "|CFF848484The infamous " << player->GetName() << " and his group slayed " << boss->GetName() << "!";
				sWorld->SendServerMessage(SERVER_MSG_STRING, ss.str().c_str());
			}
			else
			{
				std::ostringstream ss;
				ss << "|CFF848484The infamous " << player->GetName() << " and her group slayed " << boss->GetName() << "!";
				sWorld->SendServerMessage(SERVER_MSG_STRING, ss.str().c_str());
			}
		}
	}
};

void AddSC_Boss_Announcer()
{
	new Boss_Announcer;
}