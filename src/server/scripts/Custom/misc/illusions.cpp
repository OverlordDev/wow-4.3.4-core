//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"

#define item_id1 500050		//used
#define item_id2 500052		//used
#define item_id3 500053		//used
#define item_id4 500054		//used
#define item_id5 500055		//used
#define item_id6 500056		//used
#define item_id7 500057
#define item_id8 500058
#define item_id9 500059
#define item_id10 500060
#define item_id11 500061
#define item_id12 500062
#define item_id13 500063
#define item_id14 500064

class ilusion_broken2 : public ItemScript
{
public:
	ilusion_broken2() : ItemScript("ilusion_broken2") {}
	bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/) override
	{
		switch (item->GetEntry())
		{
		case item_id1:
			player->SetDisplayId(22642); //Magister's Terrace - Selin Fireheart (1st boss drop)
			break;
		case item_id2:
			player->SetDisplayId(23092); //Magister's Terrace - Sunblade Keeper (2nd boss drop)
			break;
		case item_id3:
			player->SetDisplayId(22541); //Magister's Terrace - Apoko (3rd boss drop)
			break;
		case item_id4:
			player->SetDisplayId(22539); //Magister's Terrace - Eramas Brightblaze (3rd boss drop)
			break;
		case item_id5:
			player->SetDisplayId(23466); // Magister's Terrace - Kael'thas Sunstrider Image (last boss)
			break;
		case item_id6:
			player->SetDisplayId(32145); // Voting Shop - Professional Bob Image
			break;
		}
		return true;
	}
};

void AddSC_ilusion_broken2()
{
	new ilusion_broken2();
}