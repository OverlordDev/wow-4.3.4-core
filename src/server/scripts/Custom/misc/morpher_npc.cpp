//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"
#include "Object.h"
#include "Chat.h"

class morpher_npc : public CreatureScript
{
public:
	morpher_npc() : CreatureScript("morpher_npc") { }

	bool OnGossipHello(Player * player, Creature * creature)
	{
		if (player->GetSession()->GetSecurity() == 0)
		{
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_pvp_a_a:40|t Alliance Races|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_pvp_h_h:40|t Horde Races|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
			//player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_pvp_o_01:35 |tTier I Morphs|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3);
			{
				if (player->HasAchieved(1159) || player->HasAchieved(1160))
					player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_arena_2v2_6:40|t |cff008000F 2.2k Arena Rating *UNLOCKED*|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 45);
				else
					player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_arena_2v2_6:40|t |cffff0000 2.2k Arena Rating *LOCKED*|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 46);
			}
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Spell_arcane_mindmastery:40|t Demorph|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 36);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707 Goodbye!|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4);
			player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		}

		else
		{
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_pvp_a_a:40|t Alliance Races|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_pvp_h_h:40|t Horde Races|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
			//player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_pvp_o_01:35 |tTier I Morphs|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3);
			{
				if (player->HasAchieved(1159) || player->HasAchieved(1160))
					player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_arena_2v2_6:40|t |cff008000 2.2k Arena Rating *UNLOCKED*|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 45);
				else
					player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_arena_2v2_6:40|t |cffff0000 2.2k Arena Rating *LOCKED*|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 46);
			}
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Spell_arcane_mindmastery:40 |tDemorph|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 36);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707 Goodbye!|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4);
			player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		}
		return true;

	}
	bool OnGossipSelect(Player * player, Creature * creature, uint32 sender, uint32 actions)
	{
		player->PlayerTalkClass->ClearMenus();

		if (sender != GOSSIP_SENDER_MAIN)
			return false;

		switch (actions)
		{
		case GOSSIP_ACTION_INFO_DEF + 1: /* Shows the Alliance Morphs menu */
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_boss_bazil_thredd:25 |t  Human Male|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 5);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_boss_grandwidowfaerlina:25 |t  Human Female|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 6);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_leader_king_magni_bronzebeard:25 |t  Dwarf Male|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 7);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Inv_misc_head_dwarf_02:25 |t  Dwarf Female|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 8);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\achievement_character_nightelf_Male:25 |t  Nightelf Male|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 9);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_leader_tyrande_whisperwind:25 |t  Nightelf Female|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 10);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\achievement_character_gnome_Male:25 |t  Gnome Male|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 11);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\achievement_character_gnome_Female:25 |t  Gnome Female|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 12);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_boss_exarch_maladaar:25 |t  Draenei Male|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 13);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\achievement_character_draenei_Female:25 |t  Draenei Female|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 14);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_worganhead:25 |t  Worgen Male|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 15);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Inv_mask_09:25 |t  Worgen Female|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 16);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Thumbsup:25 |t  Thank you!|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4);
			player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 2: /* Shows the Horde Morphs menu */
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_character_orc_Male:25 |t  Orc Male|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 17);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Inv_misc_head_orc_02:25 |t  Orc Female|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 18);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Inv_misc_head_undead_01:25 |t  Undead Male|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 19);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Inv_misc_head_undead_02:25 |t  Undead Female|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 20);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievment_boss_blackhorn:25 |t  Tauren Male|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 21);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Inv_misc_head_tauren_02:25 |t  Tauren Female|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 22);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_zulgurub_zanzil:25 |t  Troll Male|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 23);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\achievement_character_troll_Female:25 |t  Troll Female|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 24);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_boss_pathaleonthecalculator:25 |t  Bloodelf Male|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 25);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\achievement_character_bloodelf_Female:25 |t  Bloodelf Female|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 26);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Achievement_goblinhead:25 |t  Goblin Male|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 27);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Inv_mask_07:25 |t  Goblin Female|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 28);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Thumbsup:25 |t  Thank you!|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4);
			player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
			/*case GOSSIP_ACTION_INFO_DEF + 3: // Show Tier I Morphs
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Demon Female Belf|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 30);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Frost Troll|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 31);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Mechanical Gnome|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 32);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Human Knight|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 33);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  English Tourist Dwarf|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 34);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Father Gabriel|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 35);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Captain Rob|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 37);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Undead Nightmare|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 38);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  The Famous Rhonin|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 39);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  The Plague Zombie|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 40);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Thumbsup:25 |t  Thank you!|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4);
			player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;*/
		case GOSSIP_ACTION_INFO_DEF + 45: // Show 2.2k Unlocked Morphs
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Druid S3 [Bloodelf - Male]|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 47);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Druid S2 [Nightelf - Male]|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 48);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Hunter S2 [Orc - Female]|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 49);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Hunter T2 [Bloodelf - Male]|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 50);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Mage S4 [Bloodelf - Male]|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 51);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Mage S4 [Dwarf - Male]|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 52);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Mage S4 [Gnome - Male]|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 53);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Mage S4 [Human - Male]|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 54);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Mage S4 [Troll - Male]|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 55);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Misc_arrowright:25 |t  Mage S4 [Bloodelf - Female]|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 56);
			player->ADD_GOSSIP_ITEM(-1, "|CFF190707|TInterface\\icons\\Thumbsup:25 |t  Thank you!|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4);
			player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 4: /* Uses the Nevermind option and closes the menu */
			player->PlayerTalkClass->SendCloseGossip();
			player->GetSession()->SendNotification("Come back later!");
			break;
			/* Alliance Morph List */
		case GOSSIP_ACTION_INFO_DEF + 5: /* Human Male Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(19723);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(19723, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 6: /* Human Female Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(19724);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(19724, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 7: /* Dwarf Male Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(20317);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(20317, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 8: /* Dwarf Female Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(37918);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(37918, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 9: /* Nightelf Male Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(20318);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(20318, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 10:/* Nightelf Female Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(37919);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(37919, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 11:/* Gnome Male Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(20580);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(20580, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 12:/* Gnome Female Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(20320);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(20320, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 13:/* Draenei Male Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(37916);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(37916, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 14:/* Draenei Female Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(20323);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(20323, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 15:/* Worgen Male Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(37915);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(37915, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 16:/* Worgen Female Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(37914);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(37914, player->GetGUID());
			break;
			/* Horde Morph List */
		case GOSSIP_ACTION_INFO_DEF + 17:/* Orc Male Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(37920);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(37920, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 18:/* Orc Female Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(20316);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(20316, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 19:/* Undead Male Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(37923);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(37923, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 20:/* Undead Female Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(37924);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(37924, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 21:/* Tauren Male Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(20319);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(20319, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 22:/* Tauren Female Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(20584);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(20584, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 23:/* Troll Male Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(20321);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(20321, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 24:/* Troll Female Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(37922);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(37922, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 26:/* Bloodelf Female Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(20370);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(20370, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 25:/* Bloodelf Male Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(20368);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(20368, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 27:/* Goblin Male Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(20582);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(20582, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 28:/* Goblin Female Morph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(20583);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(20583, player->GetGUID());
			break;
			/*//tier 1 morph list
			case GOSSIP_ACTION_INFO_DEF + 30: //Demon Female Belf
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(24930);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			break;
			case GOSSIP_ACTION_INFO_DEF + 31: //Frost Troll
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(24938);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			break;
			case GOSSIP_ACTION_INFO_DEF + 32: //Mechanical Gnome
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(24127);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 0.3);
			break;
			case GOSSIP_ACTION_INFO_DEF + 33: //Human Knight
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(25500);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			break;
			case GOSSIP_ACTION_INFO_DEF + 34: //English Tourist Dwarf
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(25516);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			break;
			case GOSSIP_ACTION_INFO_DEF + 35: //Father Gabriel
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(25546);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			break;
			case GOSSIP_ACTION_INFO_DEF + 37: //Captain Rob
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(25036);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			break;
			case GOSSIP_ACTION_INFO_DEF + 38: //Undead Nightmare
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(25006);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 0.8);
			break;
			case GOSSIP_ACTION_INFO_DEF + 39: //The Famous Rhonin
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(16024);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 0.8);
			break;
			case GOSSIP_ACTION_INFO_DEF + 40: //The Plague Zombie
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(546);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			break;*/
			//arena 2,2k rating morph list
		case GOSSIP_ACTION_INFO_DEF + 47: //Druid S3 [Bloodelf - Male]
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(24806);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 0.7f);
			UpdateMorph(24806, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 48: //Druid S2 [Nightelf - Male ] 
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(28182);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(28182, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 49: //Hunter S2 [Orc - female]
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(28185);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(28185, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 50: //Hunter T2 [Bloodelf - male ] 
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(23845);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 0.7f);
			UpdateMorph(23845, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 51: //Mage S4 [Bloodelf - male ]
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(28169);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(28169, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 52: //Mage S4 [Dwarf - male ]
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(28170);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(28170, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 53: //Mage S4 [Gnome - male ]
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(28169);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(28169, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 54: //Mage S4 [Human - male ]
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(28164);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(28164, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 55: //Mage S4 [Troll - male ]
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(28203);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(28203, player->GetGUID());
			break;
		case GOSSIP_ACTION_INFO_DEF + 56: //Mage S4 [Troll - Male]
			player->PlayerTalkClass->SendCloseGossip();
			player->SetDisplayId(28168);
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(28168, player->GetGUID());
			break;
			// demorph option
		case GOSSIP_ACTION_INFO_DEF + 36:/* Demorph*/
			player->PlayerTalkClass->SendCloseGossip();
			player->DeMorph();
			player->SetFloatValue(OBJECT_FIELD_SCALE_X, 1);
			UpdateMorph(player->GetNativeDisplayId(), player->GetGUID());
			break;
		}
		return true;
	}

	void UpdateMorph(uint32 morphid, uint32 guid)
	{
		PreparedStatement * stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_MORPH_ID);
		stmt->setInt32(0, morphid);
		stmt->setInt32(1, guid);
		CharacterDatabase.Execute(stmt);
	}
};

void AddSC_morpher_npc()
{
	new morpher_npc;
}
