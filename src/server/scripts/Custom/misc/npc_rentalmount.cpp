//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"

//Mount Spells
//Flying
#define MYSTERY_QIRAJI			31700
#define INVINCIBLE_REINS		72286
#define ASHES_OF_ALAR			40192
#define STONE_DRAKE				88746
#define DRAKE_OF_NORTH			88742
#define PHOSPHORESCENT_DRAKE	88718
#define DRAKE_OF_WEST			88741

//Mount Spells
//Ground
#define SPECTRAL_TIGER			42776
#define RIDING_CAMEL		    88750
#define AMANI_WARBEAR			43688

//Money Cost
//Formula 1.000.000 = 100 Gold
#define MYSTERY_QIRAJI_COST			25000000
#define INVINCIBLE_REINS_COST		15000000
#define ASHES_OF_ALAR_COST			8500000
#define STONE_DRAKE_COST			5000000
#define DRAKE_OF_NORTH_COST			5000000
#define PHOSPHORESCENT_DRAKE_COST	5000000
#define DRAKE_OF_WEST_COST			5000000

//Mount Spells
//Ground
#define SPECTRAL_TIGER_COST			15000000
#define RIDING_CAMEL_COST		    10000000
#define AMANI_WARBEAR_COST			2500000

class npc_rentalmount : public CreatureScript
{
public:
	npc_rentalmount() : CreatureScript("npc_rentalmount") {}

	bool OnGossipHello(Player* player, Creature* creature)
	{ 
		if (!player || !creature)
			return true;

		// Flying Mounts
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_mount_spectralgryphon:25|tRent me a Mystery Qiraji (2500g)", GOSSIP_SENDER_MAIN, 101);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_mount_spectralgryphon:25|tRent me a Invincible Reins (1500g)", GOSSIP_SENDER_MAIN, 102);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_mount_spectralgryphon:25|tRent me a Ashes of Al'ar (850g)", GOSSIP_SENDER_MAIN, 103);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_mount_spectralgryphon:25|tRent me a Stone Drake (500g)", GOSSIP_SENDER_MAIN, 104);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_mount_spectralgryphon:25|tRent me a Drake of North (500g)", GOSSIP_SENDER_MAIN, 105);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_mount_spectralgryphon:25|tRent me a Phosphorescent (500g)", GOSSIP_SENDER_MAIN, 106);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_mount_spectralgryphon:25|tRent me a Drake of West (500g)", GOSSIP_SENDER_MAIN, 107);

		// Ground Mounts
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_mount_cockatricemount_purple:25|tRent me a Spectral Tiger (1500g)", GOSSIP_SENDER_MAIN, 108);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_mount_cockatricemount_purple:25|tRent me a Riding Camel (1000g)", GOSSIP_SENDER_MAIN, 109);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_mount_cockatricemount_purple:25|tRent me a Amani Warbear (250g)", GOSSIP_SENDER_MAIN, 110);
	 
		player->ADD_GOSSIP_ITEM(4, "|CFF190707 Nevermind!|r", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4);
		player->SEND_GOSSIP_MENU(100001, creature->GetGUID());
	    return true; 
	} 
	
	bool OnGossipSelect(Player* Player, Creature* creature, uint32 /*uiSender*/, uint32 uiAction)
	{ 
		WorldSession* ws = Player->GetSession();

		if (!Player || !creature)
			return true;
	
	    if (Player->IsMounted()){
			ws->SendNotification("You alredy have an active mount!");
	         return false; 
	    } 
		
		switch (uiAction)
	    { 
		case GOSSIP_ACTION_INFO_DEF + 4: // Close the menu
			Player->PlayerTalkClass->SendCloseGossip();
			Player->GetSession()->SendNotification("Come visit me regularly you might find new mounts!");
			break;

		case 101:
			if (Player->GetMoney() < 25000000) // 2500 gold
			{
				ws->SendNotification("You do not have enough money, farm more gold and come back!");
			}
			else
			{
				Player->RemoveAurasDueToSpell(66916); // Remove aura before just in case
				Player->AddAura(66916, Player); // You feel very happy
				Player->AddAura(MYSTERY_QIRAJI, Player);
				Player->ModifyMoney(-MYSTERY_QIRAJI_COST);
				ChatHandler(Player->GetSession()).PSendSysMessage("|cffFFFFFF2500 Gold withdrawn from your balance.");
			}
			break;
	    case 102: 
			if (Player->GetMoney() < 15000000) // 1500 gold
	        { 
				ws->SendNotification("You do not have enough money, farm more gold and come back!");
	        } 
			else 
			{ 
				Player->RemoveAurasDueToSpell(66916); // Remove aura before just in case
				Player->AddAura(66916, Player); // You feel very happy
				Player->AddAura(INVINCIBLE_REINS, Player);
				Player->ModifyMoney(-INVINCIBLE_REINS_COST);
				ChatHandler(Player->GetSession()).PSendSysMessage("|cffFFFFFF1500 Gold withdrawn from your balance.");
	        } 
	        break; 
		case 103:
			if (Player->GetMoney() < 8500000) // 850 gold
			{
				ws->SendNotification("You do not have enough money, farm more gold and come back!");
			}
			else
			{
				Player->RemoveAurasDueToSpell(66916); // Remove aura before just in case
				Player->AddAura(66916, Player); // You feel very happy
				Player->AddAura(ASHES_OF_ALAR, Player);
				Player->ModifyMoney(-ASHES_OF_ALAR_COST);
				ChatHandler(Player->GetSession()).PSendSysMessage("|cffFFFFFF850 Gold withdrawn from your balance.");
			}
			break;
		case 104:
			if (Player->GetMoney() < 5000000) // 500 gold
			{
				ws->SendNotification("You do not have enough money, farm more gold and come back!");
			}
			else
			{
				Player->RemoveAurasDueToSpell(66916); // Remove aura before just in case
				Player->AddAura(66916, Player); // You feel very happy
				Player->AddAura(STONE_DRAKE, Player);
				Player->ModifyMoney(-STONE_DRAKE_COST);
				ChatHandler(Player->GetSession()).PSendSysMessage("|cffFFFFFF500 Gold withdrawn from your balance.");
			}
			break;
		case 105:
			if (Player->GetMoney() < 5000000) // 500 gold
			{
				ws->SendNotification("You do not have enough money, farm more gold and come back!");
			}
			else
			{
				Player->RemoveAurasDueToSpell(66916); // Remove aura before just in case
				Player->AddAura(66916, Player); // You feel very happy
				Player->AddAura(DRAKE_OF_NORTH, Player);
				Player->ModifyMoney(-DRAKE_OF_NORTH_COST);
				ChatHandler(Player->GetSession()).PSendSysMessage("|cffFFFFFF500 Gold withdrawn from your balance.");
			}
			break;
		case 106:
			if (Player->GetMoney() < 5000000) // 500 gold
			{
				ws->SendNotification("You do not have enough money, farm more gold and come back!");
			}
			else
			{
				Player->RemoveAurasDueToSpell(66916); // Remove aura before just in case
				Player->AddAura(66916, Player); // You feel very happy
				Player->AddAura(PHOSPHORESCENT_DRAKE, Player);
				Player->ModifyMoney(-PHOSPHORESCENT_DRAKE_COST);
				ChatHandler(Player->GetSession()).PSendSysMessage("|cffFFFFFF500 Gold withdrawn from your balance.");
			}
			break;
		case 107:
			if (Player->GetMoney() < 5000000) // 500 gold
			{
				ws->SendNotification("You do not have enough money, farm more gold and come back!");
			}
			else
			{
				Player->RemoveAurasDueToSpell(66916); // Remove aura before just in case
				Player->AddAura(66916, Player); // You feel very happy
				Player->AddAura(DRAKE_OF_WEST, Player);
				Player->ModifyMoney(-DRAKE_OF_WEST_COST);
				ChatHandler(Player->GetSession()).PSendSysMessage("|cffFFFFFF500 Gold withdrawn from your balance.");
			}
			break;
		case 108:
			if (Player->GetMoney() < 15000000) // 1500 gold
			{
				ws->SendNotification("You do not have enough money, farm more gold and come back!");
			}
			else
			{
				Player->RemoveAurasDueToSpell(66916); // Remove aura before just in case
				Player->AddAura(66916, Player); // You feel very happy
				Player->AddAura(SPECTRAL_TIGER, Player);
				Player->ModifyMoney(-SPECTRAL_TIGER_COST);
				ChatHandler(Player->GetSession()).PSendSysMessage("|cffFFFFFF1500 Gold withdrawn from your balance.");
			}
			break;
		case 109:
			if (Player->GetMoney() < 10000000) // 1000 gold
			{
				ws->SendNotification("You do not have enough money, farm more gold and come back!");
			}
			else
			{
				Player->RemoveAurasDueToSpell(66916); // Remove aura before just in case
				Player->AddAura(66916, Player); // You feel very happy
				Player->AddAura(RIDING_CAMEL, Player);
				Player->ModifyMoney(-RIDING_CAMEL_COST);
				ChatHandler(Player->GetSession()).PSendSysMessage("|cffFFFFFF1000 Gold withdrawn from your balance.");
			}
			break;
		case 110:
			if (Player->GetMoney() < 2500000) // 250 gold
			{
				ws->SendNotification("You do not have enough money, farm more gold and come back!");
			}
			else
			{
				Player->RemoveAurasDueToSpell(66916); // Remove aura before just in case
				Player->AddAura(66916, Player); // You feel very happy
				Player->AddAura(AMANI_WARBEAR, Player);
				Player->ModifyMoney(-AMANI_WARBEAR_COST);
				ChatHandler(Player->GetSession()).PSendSysMessage("|cffFFFFFF250 Gold withdrawn from your balance.");
			}
			break;

			OnGossipHello(Player, creature);
	    } 	
		return true;
	} 
};

void AddSC_npc_rentalmount()
{
	new npc_rentalmount;
}