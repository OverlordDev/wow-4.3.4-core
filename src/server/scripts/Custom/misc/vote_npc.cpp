//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"

class npc_vote : public CreatureScript
{
public:
	npc_vote() : CreatureScript("npc_vote") { }

	bool OnGossipHello(Player * player, Creature * creature)
	{
		std::stringstream infoPoints;
		guid = player->GetGUID();
		QueryResult guid_db = CharacterDatabase.PQuery("SELECT account FROM characters WHERE guid = %u", guid);
		account = guid_db->Fetch()[0].GetInt32();
		QueryResult vote_db = LoginDatabase.PQuery("SELECT vp FROM account WHERE id = %u", account);
		votepoints = vote_db->Fetch()[0].GetInt32();
		infoPoints << "Total Vote Coins: " << "|cFF009933" << votepoints << "|r" << std::endl;

		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, infoPoints.str().c_str(), GOSSIP_SENDER_MAIN, 0);
		player->ADD_GOSSIP_ITEM(10, "|TInterface\\icons\\Achievement_general_100kdailyquests:30|t Category 1 -> 50 vote coins", GOSSIP_SENDER_MAIN, 1);
		player->ADD_GOSSIP_ITEM(10, "|TInterface\\icons\\Achievement_general_100kdailyquests:30|t Category 2 -> 100 vote coins", GOSSIP_SENDER_MAIN, 2);
		player->ADD_GOSSIP_ITEM(10, "|TInterface\\icons\\Achievement_general_100kdailyquests:30|t Category 3 -> 150 vote coins", GOSSIP_SENDER_MAIN, 3);
		player->ADD_GOSSIP_ITEM(10, "|TInterface\\icons\\Achievement_general_100kdailyquests:30|t Category 4 -> 250 vote coins", GOSSIP_SENDER_MAIN, 4);
		player->ADD_GOSSIP_ITEM(10, "|TInterface\\icons\\Achievement_general_100kdailyquests:30|t Category 5 -> 500 vote coins", GOSSIP_SENDER_MAIN, 5);
		player->ADD_GOSSIP_ITEM(10, "|TInterface\\icons\\Achievement_general_100kdailyquests:30|t Category 6 -> 800 vote coins", GOSSIP_SENDER_MAIN, 6);
		player->PlayerTalkClass->SendGossipMenu(61616, creature->GetGUID());
        return true;
	}
		/*if (votepoints >= 50)
		{
			player->ADD_GOSSIP_ITEM(-1, "Category 1-> 50 vote coins", GOSSIP_SENDER_MAIN, FIFTY_COINS);
		}*/

	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		uint32 guid, account, votepoints;
		std::stringstream buffer;
		guid = player->GetGUID();
		QueryResult guid_db = CharacterDatabase.PQuery("SELECT account FROM characters WHERE guid = %u", guid);
		account = guid_db->Fetch()[0].GetInt32();
		QueryResult vote_db = LoginDatabase.PQuery("SELECT vp FROM account WHERE id = %u", account);
		votepoints = vote_db->Fetch()[0].GetInt32();

		player->PlayerTalkClass->ClearMenus();

		if (sender == GOSSIP_SENDER_MAIN)
		{
			switch (action)
			{
				// 50 Vote Coins
				case 1:
					player->ADD_GOSSIP_ITEM_EXTENDED(10, "|TInterface\\icons\\Achievement_character_dwarf_female:25|t Race Change Token x2", GOSSIP_SENDER_MAIN, 50, "Are you sure?", 0, false);
					player->ADD_GOSSIP_ITEM_EXTENDED(10, "|TInterface\\icons\\Achievement_cloudnine:25|t Faction Change Token x1", GOSSIP_SENDER_MAIN, 51, "Are you sure?", 0, false);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, -1);
					player->PlayerTalkClass->SendGossipMenu(61617, creature->GetGUID());
					break;
					// Race Change Token x2
					case 50:
						if (votepoints >= 50)
						{
							player->AddItem(150001, 2);
							UpdateVotepoints(account, votepoints - 50);
							creature->MonsterWhisper("|cFF009933|rYou just spent -50 Vote Coins to buy a Race Change Token!", player->GetGUID(), true);
							player->CLOSE_GOSSIP_MENU();
						}
						else
						{
							creature->MonsterWhisper("|c00077766|r Not enough Vote Coins, visit our website and vote every 12 hours!", player->GetGUID(), true);
							player->CLOSE_GOSSIP_MENU();
						}
						break;
					// Faction Change Token x1
					case 51:
						if (votepoints >= 50)
						{
							player->AddItem(150000, 1);
							UpdateVotepoints(account, votepoints - 50);
							creature->MonsterWhisper("|cFF009933|rYou just spent -50 Vote Coins to buy a Faction Change Token!", player->GetGUID(), true);
							player->CLOSE_GOSSIP_MENU();
						}
						else
						{
							creature->MonsterWhisper("|c00077766|r Not enough Vote Coins, visit our website and vote every 12 hours!", player->GetGUID(), true);
							player->CLOSE_GOSSIP_MENU();
						}
						break;
				// 100 Vote Coins
				case 2:
					player->ADD_GOSSIP_ITEM_EXTENDED(10, "|TInterface\\icons\\Inv_shirt_01:25|t Voting Custom Shirt I x1", GOSSIP_SENDER_MAIN, 100, "Are you sure?", 0, false);
					player->ADD_GOSSIP_ITEM_EXTENDED(10, "|TInterface\\icons\\Ability_racial_twoforms:25|t Professional Bob Image x1", GOSSIP_SENDER_MAIN, 101, "Are you sure?", 0, false);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, -1);
					player->PlayerTalkClass->SendGossipMenu(61618, creature->GetGUID());
					break;
					// Voting Shirt I x1
					case 100:
						if (votepoints >= 100)
						{
							player->AddItem(140000, 1);
							UpdateVotepoints(account, votepoints - 100);
							creature->MonsterWhisper("|cFF009933|rYou just spent -100 Vote Coins to buy a custom effects Shirt!", player->GetGUID(), true);
							player->CLOSE_GOSSIP_MENU();
						}
						else
						{
							creature->MonsterWhisper("|c00077766|r Not enough Vote Coins, visit our website and vote every 12 hours!", player->GetGUID(), true);
							player->CLOSE_GOSSIP_MENU();
						}
						break;
					// Voting Custom Illusion - 1 Professional Bob
					case 101:
						if (votepoints >= 100)
						{
							player->AddItem(500056, 1);
							UpdateVotepoints(account, votepoints - 100);
							creature->MonsterWhisper("|cFF009933|rYou just spent -100 Vote Coins to buy a Professional Bob Image!", player->GetGUID(), true);
							player->CLOSE_GOSSIP_MENU();
						}
						else
						{
							creature->MonsterWhisper("|c00077766|r Not enough Vote Coins, visit our website and vote every 12 hours!", player->GetGUID(), true);
							player->CLOSE_GOSSIP_MENU();
						}
						break;
				// 150 Vote Coins
				case 3:
					player->ADD_GOSSIP_ITEM_EXTENDED(10, "|TInterface\\icons\\Trade_archaeology_silverdagger:25|t Thunderfury Dagger (not bound) x1", GOSSIP_SENDER_MAIN, 150, "Are you sure?", 0, false);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, -1);
					player->PlayerTalkClass->SendGossipMenu(61619, creature->GetGUID());
					break;
					// Thunderfury dagger x1
					case 150:
						if (votepoints >= 150)
						{
							player->AddItem(450660, 1);
							UpdateVotepoints(account, votepoints - 150);
							creature->MonsterWhisper("|cFF009933|rYou just spent -150 Vote Coins to buy a Thunderfury dagger!", player->GetGUID(), true);
							player->CLOSE_GOSSIP_MENU();
						}
						else
						{
							creature->MonsterWhisper("|c00077766|r Not enough Vote Coins, visit our website and vote every 12 hours!", player->GetGUID(), true);
							player->CLOSE_GOSSIP_MENU();
						}
						break;
				// 250 Vote Coins
				case 4:
					player->ADD_GOSSIP_ITEM_EXTENDED(10, "|TInterface\\icons\\Inv_box_02:25|t Tmog Box (Weapons) x1", GOSSIP_SENDER_MAIN, 250, "Are you sure?", 0, false);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, -1);
					player->PlayerTalkClass->SendGossipMenu(61620, creature->GetGUID());
					break;
					// Tmog box weapons x1
					case 250:
						if (votepoints >= 250)
						{
							player->AddItem(305012, 1);
							UpdateVotepoints(account, votepoints - 250);
							creature->MonsterWhisper("|cFF009933|rYou just spent -250 Vote Coins to buy a Tmog weapons box!", player->GetGUID(), true);
							player->CLOSE_GOSSIP_MENU();
						}
						else
						{
							creature->MonsterWhisper("|c00077766|r Not enough Vote Coins, visit our website and vote every 12 hours!", player->GetGUID(), true);
							player->CLOSE_GOSSIP_MENU();
						}
						break;
				// 500 Vote Coins
				case 5:
					player->ADD_GOSSIP_ITEM_EXTENDED(10, "|TInterface\\icons\\Ability_mount_gyrocoptor:25|t Vote Machine Mount x1", GOSSIP_SENDER_MAIN, 500, "Are you sure?", 0, false);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, -1);
					player->PlayerTalkClass->SendGossipMenu(61621, creature->GetGUID());
					break;
					// Vote Machine Mount x1
					case 500:
						if (votepoints >= 500)
						{
							player->AddItem(997850, 1);
							UpdateVotepoints(account, votepoints - 500);
							creature->MonsterWhisper("|cFF009933|rYou just spent -500 Vote Coins to buy a Vote Machine Mount!", player->GetGUID(), true);
							player->CLOSE_GOSSIP_MENU();
						}
						else
						{
							creature->MonsterWhisper("|c00077766|r Not enough Vote Coins, visit our website and vote every 12 hours!", player->GetGUID(), true);
							player->CLOSE_GOSSIP_MENU();
						}
						break;
				// 800 Vote Coins
				case 6:
					player->ADD_GOSSIP_ITEM_EXTENDED(10, "|TInterface\\icons\\Achievement_guildperk_mrpopularity:25|t V.I.P. Account R1 (forever) x1", GOSSIP_SENDER_MAIN, 800, "Are you sure?", 0, false);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "<-Back", GOSSIP_SENDER_MAIN, -1);
					player->PlayerTalkClass->SendGossipMenu(61622, creature->GetGUID());
					break;
					// Vote Machine Mount x1
					case 800:
						if (votepoints >= 800)
						{
							player->AddItem(777776, 1);
							UpdateVotepoints(account, votepoints - 800);
							creature->MonsterWhisper("|cFF009933|rYou just spent -800 Vote Coins to buy a V.I.P. Account rank 1!", player->GetGUID(), true);
							player->CLOSE_GOSSIP_MENU();
						}
						else
						{
							creature->MonsterWhisper("|c00077766|r Not enough Vote Coins, visit our website and vote every 12 hours!", player->GetGUID(), true);
							player->CLOSE_GOSSIP_MENU();
						}
						break;
				// Main Page
				case -1:
					OnGossipHello(player, creature);
					break;
			}
		}
		return true;
	}

	void UpdateVotepoints(uint32 account, uint32 votepoints)
	{
		PreparedStatement * stmt = LoginDatabase.GetPreparedStatement(LOGIN_UPDATE_ACCOUNT_VP);
		stmt->setInt32(0, votepoints);
		stmt->setInt32(1, account);
		LoginDatabase.Execute(stmt);

	}

	private:
		uint32 guid, account, votepoints;	
};

void AddSC_npc_vote()
{
	new npc_vote();
}