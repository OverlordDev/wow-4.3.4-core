//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"
#include "Player.h"
enum ListOfBosses
{
	BOSS_DIPPY                    = 333394,
	BOSS_BILL_THE_JANITOR         = 333415,
	BOSS_MURKIMUS_THE_GLADIATOR   = 333398,
	BOSS_HARRISON_JONES           = 333403,
	BOSS_GAMON                    = 333406,
	BOSS_SHARKWAVE                = 333408,
	BOSS_GRANDPA_PUDDLES          = 333416,
	BOSS_LOST_VIKINGS_1           = 333399,
	BOSS_LOST_VIKINGS_2           = 333400,
	BOSS_LOST_VIKINGS_3           = 333401,
	BOSS_NAT_PAGLE                = 333404,
	BOSS_HOGGER                   = 333405,
	BOSS_BATTLE_CHICKEN           = 333413,
	BOSS_NEEDLESPINE_COBRA        = 333409,
	BOSS_PEPPY                    = 333412,
	BOSS_LAVA_WORM                = 333417,
	BOSS_MASTER_PAKU              = 400026,
	BOSS_MINI_THOR                = 333407,
	BOSS_VALEERA_SANGUINAR        = 333402,
	BOSS_XT_DECONSTRUCTOR         = 333414,
	CREATURE_HARRISON_CANNON      = 333427,

	// Map ID
	MAP_ID                        = 650,

	// Spell
	TELEPORT_VISUAL_SPELL         = 83369
};

class brawler_guild : public PlayerScript
{
public:
	brawler_guild() : PlayerScript("brawler_guild") { }

	void OnPlayerKilledByCreature(Creature* killer, Player* killed)
	{
		uint32 entry = killer->GetEntry();
		uint32 maxhealth = killed->GetMaxHealth();
		switch (entry)
		{
		case BOSS_DIPPY:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}
		
		case BOSS_BILL_THE_JANITOR:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_MURKIMUS_THE_GLADIATOR:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_HARRISON_JONES:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_GAMON:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_SHARKWAVE:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_GRANDPA_PUDDLES:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_LOST_VIKINGS_1:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_LOST_VIKINGS_2:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_LOST_VIKINGS_3:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_NAT_PAGLE:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_HOGGER:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_BATTLE_CHICKEN:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_NEEDLESPINE_COBRA:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_PEPPY:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_LAVA_WORM:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_MASTER_PAKU:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_MINI_THOR:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case BOSS_VALEERA_SANGUINAR:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}
		
		case BOSS_XT_DECONSTRUCTOR:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}

		case CREATURE_HARRISON_CANNON:
		{
			killed->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			killed->ResurrectPlayer(1.0f);
			killed->GetSession()->SendNotification("Better luck next time!");
			killed->CastSpell(killed, TELEPORT_VISUAL_SPELL, true);
		}
		}
	}

/*	void OnLogin(Player* player)             //use only when map becomes open world
	{
		if (player->GetMapId() == MAP_ID)
			if (player->
		{
			player->TeleportTo(650, 720.83f, 554.75f, 439.0f, 1.26f);
			player->CastSpell(player, TELEPORT_VISUAL_SPELL, true);
		}
	}
	*/
};

void AddSC_brawler_guild()
{
	new brawler_guild();
}