//Syndicate-WoW | Cataclysm

#include "ScriptPCH.h"

// First we need to setup all the codes for the 10 digits as matrices...like so
uint32 Zero[5][4] =         // Zero
{
	{ 1, 1, 1, 1 },
	{ 1, 0, 0, 1 },
	{ 1, 0, 0, 1 },
	{ 1, 0, 0, 1 },
	{ 1, 1, 1, 1 },
};

uint32 One[5][4] =          // One
{
	{ 0, 0, 0, 1 },
	{ 0, 0, 0, 1 },
	{ 0, 0, 0, 1 },
	{ 0, 0, 0, 1 },
	{ 0, 0, 0, 1 },
};

uint32 Two[5][4] =          // Two
{
	{ 1, 1, 1, 1 },
	{ 1, 0, 0, 0 },
	{ 1, 1, 1, 1 },
	{ 0, 0, 0, 1 },
	{ 1, 1, 1, 1 },
};

uint32 Three[5][4] =        // Three
{
	{ 1, 1, 1, 1 },
	{ 0, 0, 0, 1 },
	{ 1, 1, 1, 1 },
	{ 0, 0, 0, 1 },
	{ 1, 1, 1, 1 },
};

uint32 Four[5][4] =        // Four
{
	{ 0, 0, 0, 1 },
	{ 0, 0, 0, 1 },
	{ 1, 1, 1, 1 },
	{ 1, 0, 0, 1 },
	{ 1, 0, 0, 1 },
};

uint32 Five[5][4] =       // Five
{
	{ 1, 1, 1, 1 },
	{ 0, 0, 0, 1 },
	{ 1, 1, 1, 1 },
	{ 1, 0, 0, 0 },
	{ 1, 1, 1, 1 },
};

uint32 Six[5][4] =        // Six
{
	{ 1, 1, 1, 1 },
	{ 1, 0, 0, 1 },
	{ 1, 1, 1, 1 },
	{ 1, 0, 0, 0 },
	{ 1, 1, 1, 1 },
};

uint32 Seven[5][4] =       // Seven
{
	{ 0, 0, 0, 1 },
	{ 0, 0, 0, 1 },
	{ 0, 0, 0, 1 },
	{ 0, 0, 0, 1 },
	{ 1, 1, 1, 1 },
};

uint32 Eight[5][4] =       // Eight
{
	{ 1, 1, 1, 1 },
	{ 1, 0, 0, 1 },
	{ 1, 1, 1, 1 },
	{ 1, 0, 0, 1 },
	{ 1, 1, 1, 1 },
};

uint32 Nine[5][4] =         // Nine 
{
	{ 1, 1, 1, 1 },
	{ 0, 0, 0, 1 },
	{ 1, 1, 1, 1 },
	{ 1, 0, 0, 1 },
	{ 1, 1, 1, 1 },
};

uint32 Dots[] =    // Dots
{ 0, 1, 0, 1, 0 };

enum Events
{
	// Seconds events (triggered every one second)
	EVENT_ZERO_SECOND       = 1,
	EVENT_ONE_SECOND        = 2,
	EVENT_TWO_SECOND        = 3,
	EVENT_THREE_SECOND      = 4,
	EVENT_FOUR_SECOND       = 5,
	EVENT_FIVE_SECOND       = 6,
	EVENT_SIX_SECOND        = 7,
	EVENT_SEVEN_SECOND      = 8,
	EVENT_EIGHT_SECOND      = 9,
	EVENT_NINE_SECOND       = 10,
	
	// Ten-Seconds events ( triggered every ten seconds )
	EVENT_DUMMY   =  11,
	EVENT_ONE_TEN_SECOND    = 12,
	EVENT_TWO_TEN_SECOND    = 13,
	EVENT_THREE_TEN_SECOND  = 14,
	EVENT_FOUR_TEN_SECOND   = 15,
	EVENT_FIVE_TEN_SECOND   = 16,
	EVENT_SIX_TEN_SECOND    = 17,
	EVENT_ZERO_TEN_SECOND	= 18,	
	// Minute events (triggered every minute)
	EVENT_ZERO_MINUTE       = 19,
	EVENT_ONE_MINUTE        = 20,
	EVENT_TWO_MINUTE        = 21,
	EVENT_THREE_MINUTE      = 22,

	EVENT_DOTS_GENERAL      = 23,

	EVENT_FINAL_DESPAWN     = 24


};
///////////////Main Idea///////////
/*
So the idea is this: we have 3 regions  of interest where imps will spawn, since the clock will be something like
 3:20, or 7:14 and such, one digit to the left and two digits to the right.
 Our index on the x axis runs from 0 to 15(included), and makes the indices i = 4, 6, 11 be NULL since they are spaces
 between imp groups.

 Next on, the algorithm for spawning imps is:

 if (i < 8)
 {
 GetCaster()->GetPosition(x, y, z);
 GetCaster()->SummonCreature(entry, x + (8 - i) * std::cos(GetCaster()->GetOrientation() + 1.55f), y + (8 - i) * std::sin(GetCaster()->GetOrientation() + 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_DEAD_DESPAWN);
 }
 if ( i >= 8)
 {
 GetCaster()->GetPosition(x, y, z);
 GetCaster()->SummonCreature(entry, x + (i - 8) * std::cos(GetCaster()->GetOrientation() - 1.55f), y + (i - 8) * std::sin(GetCaster()->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_DEAD_DESPAWN);
 }

 Where for the the digits, our index is:

 I.   First digit  ->   i = 0,3;
 II.  Second digit ->   i = 7,10;
 III. Third digit  ->	i = 12,15;

 &Extra:
 The two dots are located at index i = 5, and they have their own spawning vector ( to be initialized when script starts
 and despawned at the end )
*/
class imp_clock : public CreatureScript
{
public:
	imp_clock() : CreatureScript("imp_clock") { }
	struct imp_clockAI : public WorldBossAI
	{
		imp_clockAI(Creature* creature) : WorldBossAI(creature) { }

		void Reset()
		{
			_Reset();
			events.ScheduleEvent(EVENT_ZERO_SECOND, 10000);
			events.ScheduleEvent(EVENT_ONE_SECOND, 9000);
			events.ScheduleEvent(EVENT_TWO_SECOND, 8000);
			events.ScheduleEvent(EVENT_THREE_SECOND, 7000);
			events.ScheduleEvent(EVENT_FOUR_SECOND, 6000);
			events.ScheduleEvent(EVENT_FIVE_SECOND, 5000);
			events.ScheduleEvent(EVENT_SIX_SECOND, 4000);
			events.ScheduleEvent(EVENT_SEVEN_SECOND, 3000);
			events.ScheduleEvent(EVENT_EIGHT_SECOND, 2000);
			events.ScheduleEvent(EVENT_NINE_SECOND, 1000);
			events.ScheduleEvent(EVENT_DUMMY, 0);
			events.ScheduleEvent(EVENT_ONE_TEN_SECOND, 41000);
			events.ScheduleEvent(EVENT_TWO_TEN_SECOND, 31000);
			events.ScheduleEvent(EVENT_THREE_TEN_SECOND, 21000);
			events.ScheduleEvent(EVENT_FOUR_TEN_SECOND, 11000);
			events.ScheduleEvent(EVENT_FIVE_TEN_SECOND, 1000);
			//events.ScheduleEvent(EVENT_SIX_TEN_SECOND, 70000);
			events.ScheduleEvent(EVENT_ZERO_TEN_SECOND, 51000);


			events.ScheduleEvent(EVENT_THREE_MINUTE, 0);
			events.ScheduleEvent(EVENT_ZERO_MINUTE, 121000);
			events.ScheduleEvent(EVENT_ONE_MINUTE, 61000);
			events.ScheduleEvent(EVENT_TWO_MINUTE, 1000);

			events.ScheduleEvent(EVENT_DOTS_GENERAL, 0);
			events.ScheduleEvent(EVENT_FINAL_DESPAWN, 181000);
		}

		void UpdateAI(uint32 const diff)
		{
			if (me->isInCombat())
				return;

			events.Update(diff);
			
			while (uint32 eventId = events.ExecuteEvent())
			{
				uint32 entry = 333351;
				float x, y, z, spacing;
				spacing = 0.0f;
				me->GetPosition(x, y, z);
				switch (eventId)
				{

				case EVENT_ZERO_SECOND:
					events.ScheduleEvent(EVENT_ZERO_SECOND, 10000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 12; j <= 15; j += 1)
						{
							if (Zero[i][j - 12] == 1)
								me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 1000);
						}
						spacing += 1;
					}
					break;

				case EVENT_ONE_SECOND:
					events.ScheduleEvent(EVENT_ONE_SECOND, 10000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 12; j <= 15; j += 1)
						{
							if (One[i][j - 12] == 1)
								me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 1000);
						}
						spacing += 1;
					}
					break;

				case EVENT_TWO_SECOND:
					events.ScheduleEvent(EVENT_TWO_SECOND, 10000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 12; j <= 15; j += 1)
						{
							if (Two[i][j - 12] == 1)
								me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 1000);
						}
						spacing += 1;
					}
					break;

				case EVENT_THREE_SECOND:
					events.ScheduleEvent(EVENT_THREE_SECOND, 10000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 12; j <= 15; j += 1)
						{
							if (Three[i][j - 12] == 1)
								me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 1000);
						}
						spacing += 1;
					}
					break;

				case EVENT_FOUR_SECOND:
					events.ScheduleEvent(EVENT_FOUR_SECOND, 10000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 12; j <= 15; j += 1)
						{
							if (Four[i][j - 12] == 1)
								me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 1000);
						}
						spacing += 1;
					}
					break;

				case EVENT_FIVE_SECOND:
					events.ScheduleEvent(EVENT_FIVE_SECOND, 10000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 12; j <= 15; j += 1)
						{
							if (Five[i][j - 12] == 1)
								me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 1000);
						}
						spacing += 1;
					}
					break;

				case EVENT_SIX_SECOND:
					events.ScheduleEvent(EVENT_SIX_SECOND, 10000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 12; j <= 15; j += 1)
						{
							if (Six[i][j - 12] == 1)
								me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 1000);
						}
						spacing += 1;
					}
					break;

				case EVENT_SEVEN_SECOND:
					events.ScheduleEvent(EVENT_SEVEN_SECOND, 10000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 12; j <= 15; j += 1)
						{
							if (Seven[i][j - 12] == 1)
								me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 1000);
						}
						spacing += 1;
					}
					break;

				case EVENT_EIGHT_SECOND:
					events.ScheduleEvent(EVENT_EIGHT_SECOND, 10000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 12; j <= 15; j += 1)
						{
							if (Eight[i][j - 12] == 1)
								me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 1000);
						}
						spacing += 1;
					}
					break;

				case EVENT_NINE_SECOND:
					events.ScheduleEvent(EVENT_NINE_SECOND, 10000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 12; j <= 15; j += 1)
						{
							if (Nine[i][j - 12] == 1)
								me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 1000);
						}
						spacing += 1;
					}
					break;

				case EVENT_DUMMY:
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 12; j <= 15; j += 1)
						{
							if (Zero[i][j - 12] == 1)
								me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 1000);
						}

						for (uint32 j = 7; j <= 10; j += 1)
						{
							if (Zero[i][j - 7] == 1)
							{
								if (j < 8)
									me->SummonCreature(entry, x + (8 - j) * std::cos(me->GetOrientation() + 1.55f), y + (8 - j) * std::sin(me->GetOrientation() + 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 1000);

								if (j >= 8)
									me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 1000);
							}
						}
						spacing += 1;
					}
					break;

				case EVENT_ONE_TEN_SECOND:
					events.ScheduleEvent(EVENT_ONE_TEN_SECOND, 60000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 7; j <= 10; j += 1)
						{
							if (One[i][j - 7] == 1)
							{
								if (j < 8)
									me->SummonCreature(entry, x + (8 - j) * std::cos(me->GetOrientation() + 1.55f), y + (8 - j) * std::sin(me->GetOrientation() + 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 10000);

								if (j >= 8)
									me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 10000);
							}

						}
						spacing += 1;
					}
					break;

				case EVENT_TWO_TEN_SECOND:
					events.ScheduleEvent(EVENT_TWO_TEN_SECOND, 60000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 7; j <= 10; j += 1)
						{
							if (Two[i][j - 7] == 1)
							{
								if (j < 8)
									me->SummonCreature(entry, x + (8 - j) * std::cos(me->GetOrientation() + 1.55f), y + (8 - j) * std::sin(me->GetOrientation() + 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 10000);

								if (j >= 8)
									me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 10000);
							}

						}
						spacing += 1;
					}
					break;

				case EVENT_THREE_TEN_SECOND:
					events.ScheduleEvent(EVENT_THREE_TEN_SECOND, 60000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 7; j <= 10; j += 1)
						{
							if (Three[i][j - 7] == 1)
							{
								if (j < 8)
									me->SummonCreature(entry, x + (8 - j) * std::cos(me->GetOrientation() + 1.55f), y + (8 - j) * std::sin(me->GetOrientation() + 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 10000);

								if (j >= 8)
									me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 10000);
							}

						}
						spacing += 1;
					}
					break;

				case EVENT_FOUR_TEN_SECOND:
					events.ScheduleEvent(EVENT_FOUR_TEN_SECOND, 60000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 7; j <= 10; j += 1)
						{
							if (Four[i][j - 7] == 1)
							{
								if (j < 8)
									me->SummonCreature(entry, x + (8 - j) * std::cos(me->GetOrientation() + 1.55f), y + (8 - j) * std::sin(me->GetOrientation() + 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 10000);

								if (j >= 8)
									me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 10000);
							}

						}
						spacing += 1;
					}
					break;

				case EVENT_FIVE_TEN_SECOND:
					events.ScheduleEvent(EVENT_FIVE_TEN_SECOND, 60000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 7; j <= 10; j += 1)
						{
							if (Five[i][j - 7] == 1)
							{
								if (j < 8)
									me->SummonCreature(entry, x + (8 - j) * std::cos(me->GetOrientation() + 1.55f), y + (8 - j) * std::sin(me->GetOrientation() + 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 10000);

								if (j >= 8)
									me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 10000);
							}

						}
						spacing += 1;
					}
					break;

				case EVENT_SIX_TEN_SECOND:
					events.ScheduleEvent(EVENT_SIX_TEN_SECOND, 51000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 7; j <= 10; j += 1)
						{
							if (Six[i][j - 7] == 1)
							{
								if (j < 8)
									me->SummonCreature(entry, x + (8 - j) * std::cos(me->GetOrientation() + 1.55f), y + (8 - j) * std::sin(me->GetOrientation() + 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 10000);

								if (j >= 8)
									me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 10000);
							}

						}
						spacing += 1;
					}
					break;

				case EVENT_ZERO_TEN_SECOND:
					events.ScheduleEvent(EVENT_ZERO_TEN_SECOND, 60000);
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 7; j <= 10; j += 1)
						{
							if (Zero[i][j - 7] == 1)
							{
								if (j < 8)
									me->SummonCreature(entry, x + (8 - j) * std::cos(me->GetOrientation() + 1.55f), y + (8 - j) * std::sin(me->GetOrientation() + 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 10000);

								if (j >= 8)
									me->SummonCreature(entry, x + (j - 8) * std::cos(me->GetOrientation() - 1.55f), y + (j - 8) * std::sin(me->GetOrientation() - 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 10000);
							}
						}
						spacing += 1;
					}
					break;

				case EVENT_DOTS_GENERAL:
					for (uint32 i = 0; i < 5; i += 1)
					{
						if (Dots[i] == 1)
							me->SummonCreature(entry, x + 3 * std::cos(me->GetOrientation() + 1.55f), y + 3 * std::sin(me->GetOrientation() + 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 181000);
						spacing += 1;
					}
					break;

				case EVENT_THREE_MINUTE:
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 0; j <= 3; j += 1)
						{
							if (Three[i][j] == 1)
								me->SummonCreature(entry, x + (8 - j) * std::cos(me->GetOrientation() + 1.55f), y + (8 - j) * std::sin(me->GetOrientation() + 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 1000);
						}
						spacing += 1;
					}
					break;

				case EVENT_TWO_MINUTE:
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 0; j <= 3; j += 1)
						{
							if (Two[i][j] == 1)
								me->SummonCreature(entry, x + (8 - j) * std::cos(me->GetOrientation() + 1.55f), y + (8 - j) * std::sin(me->GetOrientation() + 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 60000);
						}
						spacing += 1;
					}
					break;

				case EVENT_ONE_MINUTE:
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 0; j <= 3; j += 1)
						{
							if (One[i][j] == 1)
								me->SummonCreature(entry, x + (8 - j) * std::cos(me->GetOrientation() + 1.55f), y + (8 - j) * std::sin(me->GetOrientation() + 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 60000);
						}
						spacing += 1;
					}
					break;

				case EVENT_ZERO_MINUTE:
					for (uint32 i = 0; i < 5; i += 1)
					{
						for (uint32 j = 0; j <= 3; j += 1)
						{
							if (Zero[i][j] == 1)
								me->SummonCreature(entry, x + (8 - j) * std::cos(me->GetOrientation() + 1.55f), y + (8 - j) * std::sin(me->GetOrientation() + 1.55f), z + spacing + 8.0f, 0, TEMPSUMMON_TIMED_DESPAWN, 60000);
						}
						spacing += 1;
					}
					break;

				case EVENT_FINAL_DESPAWN:
					me->Kill(me);
					}
				}
			}

		};

		CreatureAI * GetAI(Creature* creature) const
		{
			return new imp_clockAI(creature);
		}

	};

	void AddSC_imp_clock()
	{
		new imp_clock();
	}
