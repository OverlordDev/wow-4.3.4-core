//Syndicate-WoW | Cataclysm

/*
This is the second part of a waypoint script, it transforms a normal creature
into one that when you click it ( if it has a speciffied path ) it will add
its current possition as a new row in the waypoint_data table to the corresponding
path id ( the given one, if no path is given it adds it to the 0th )

*check path_creator.cpp

Made by Overlord
*/

#include "ScriptPCH.h"

class path_drone : public CreatureScript
{
public:
	path_drone() : CreatureScript("path_drone") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		uint32 point = 0;
		uint32 pathid = creature->GetUInt64Value(UNIT_FIELD_TARGET);
		float x, y, z;
		creature->GetPosition(x, y, z);

		PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_WAYPOINT_DATA_MAX_POINT);
		stmt->setUInt32(0, pathid);
		PreparedQueryResult result = WorldDatabase.Query(stmt);

		if (result)
			point = (*result)[0].GetUInt32();
		std::string msay = "Point " + std::to_string(point + 1);
		const char *chr = msay.c_str();
		InsertingPreparedStatement(pathid, point + 1, x, y, z);
		creature->MonsterSay(chr, 0, player->GetGUID());
		creature->SummonGameObject(19063, creature->GetPositionX(), creature->GetPositionY(), creature->GetPositionZ(), 0, 0, 0, 0, 0, 0);
		creature->SetVisible(false);
		//creature->DespawnOrUnsummon();

		return true;
	}


	void InsertingPreparedStatement(uint32 guid, uint32 id, float position_x, float position_y, float position_z)
	{
		PreparedStatement * stmt = WorldDatabase.GetPreparedStatement(WORLD_INS_WAYPOINT_PATH);
		stmt->setUInt32(0, guid);
		stmt->setUInt32(1, id);
		stmt->setFloat(2, position_x);
		stmt->setFloat(3, position_y);
		stmt->setFloat(4, position_z);
		stmt->setUInt32(5, 0);
		stmt->setUInt32(6, 0);
		stmt->setUInt32(7, 0);
		stmt->setUInt32(8, 0);
		stmt->setUInt32(9, 100);
		stmt->setUInt32(10, 0);
		stmt->setUInt32(11, 0);
		WorldDatabase.Execute(stmt);

	}

};

void AddSC_path_drone()
{
	new path_drone();
}

/*

uint32 point = 0;
uint32 pathid = creature->AI()->GetData(DATA_PATH_ID);
float x, y, z;
creature->GetPosition(x, y, z);

PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_WAYPOINT_DATA_MAX_POINT);
stmt->setUInt32(0, pathid);
PreparedQueryResult result = WorldDatabase.Query(stmt);

if (result)
point = (*result)[0].GetUInt32();

InsertingPreparedStatement(pathid, point + 1, x, y, z); */