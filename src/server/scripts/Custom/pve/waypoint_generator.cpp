//Syndicate-WoW | Cataclysm

/*
===================Waypoint Duplicating=============================
This script is designed to mimic any creature waypoint
path, and then shift it with an input distance and angle
from the original.
I also added a way through which this speciffic npc adds
the path to the databse waypoint_data table, but after some
experimentation i realised that the order of the points breaks
for complex paths or paths that have close waypoints.

Given that i added a special gossip option that lets you transform
the dummy point into a clickable point that when you click it, it will
automatically add the next point in the ladder to the speciffied path.

So you can generate the path and then go from one point to another and
click them in the order you want to points to be added in the database.

*check path_drone.cpp

Made by Overlord

*/

#include "ScriptPCH.h"
#include "Unit.h"

class path_creator : public CreatureScript
{
public:
	path_creator() : CreatureScript("path_creator") { }

	uint32 entry = 0;
	float x, y, z, o;
	float angle = 0;
	uint32 guid = 0;
	float radius = 0;
	uint32 dummy = 0;
	float actiondist = 0;
	bool OnGossipHello(Player* player, Creature* creature)
	{
		if (entry == 0)
		{
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "Enter orignal path creature entry", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1, "", 0, true);
		}

		if (entry != 0)
		{
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "Enter the new path creature entry", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2, "", 0, true);
		}
		if (entry != 0 && dummy != 0)
		{
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "Enter the radius", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3, "", 0, true);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "Enter the angle", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4, "", 0, true);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "Enter the holder action distance", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 5, "", 0, true);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Reorient all original entries", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 9);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Relocate Z Axis of Dummy", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 10);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "Clear all Spawns", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 8, "", 0, true);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "Insert path into Database", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 11, "", 0, true);
		}

		if (radius != 0)
		{
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Generate Path", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 6);
		}
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Static Reset", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 7);
		player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "Set Data to Dummy", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 13, "", 0, true);
		std::stringstream buffer;
		buffer << "Entry: " << "|cFFff0000" << entry << " " << "|r Dummy: |cFFff0000" << dummy << " |r " << std::endl;
		buffer << "Radius: " << "|cFF008ae6" << radius << "|r ft. " << " Action Distance: |cFF008ae6" << actiondist << " |r ft. " << std::endl;
		buffer << "Angle: " << "|cFFcc33ff" << angle << "|r " << " Path: |cFFcc33ff " << guid << " |r " << std::endl;
		player->ADD_GOSSIP_ITEM(-1, buffer.str(), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 12);


		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());

		return true;
	}


	bool OnGossipSelectCode(Player* player, Creature* creature, uint32 sender, uint32 action, char const* code)
	{
		std::list<Unit*> despawnable;
		Trinity::AnyFriendlyUnitInObjectRangeCheck u_check(creature, creature, actiondist);
		Trinity::UnitListSearcher<Trinity::AnyFriendlyUnitInObjectRangeCheck> searcher(creature, despawnable, u_check);
		creature->VisitNearbyObject(actiondist, searcher);
		player->PlayerTalkClass->ClearMenus();
		if (sender == GOSSIP_SENDER_MAIN)
		{
			switch (action)
			{
			case GOSSIP_ACTION_INFO_DEF + 1:
			{
				std::string codestr;
				codestr = code;
				entry = std::stoi(codestr.c_str());              // entry of the guidance creature
				player->GetSession()->SendNotification("The entry you chose set to '%u' ", entry);
				player->PlayerTalkClass->SendCloseGossip();
				break;
			}

			case GOSSIP_ACTION_INFO_DEF + 2:
			{
				std::string dummystr;
				dummystr = code;
				dummy = std::atof(dummystr.c_str());
				player->GetSession()->SendNotification("The entry of the dummy creature set to '%u'", dummy);
				player->PlayerTalkClass->SendCloseGossip();
				break;
			}
			case GOSSIP_ACTION_INFO_DEF + 3:
			{
				std::string distancestr;
				distancestr = code;
				radius = std::atof(distancestr.c_str());
				player->GetSession()->SendNotification("The radius of the new path set to '%f'", radius);
				player->PlayerTalkClass->SendCloseGossip();
				break;
			}

			case GOSSIP_ACTION_INFO_DEF + 4:
			{
				std::string anglestr;
				anglestr = code;
				angle = std::atof(anglestr.c_str());
				player->GetSession()->SendNotification("The angle w.r.t original set to '%f'", angle);
				player->PlayerTalkClass->SendCloseGossip();
				break;
			}

			case GOSSIP_ACTION_INFO_DEF + 5:
			{
				std::string actionstr;
				actionstr = code;
				actiondist = std::atof(actionstr.c_str());
				player->GetSession()->SendNotification("The holder action radius set to '%f'", actiondist);
				player->PlayerTalkClass->SendCloseGossip();
				break;
			}


			case GOSSIP_ACTION_INFO_DEF + 8:
			{
				std::string despawnstr;
				despawnstr = code;
				uint32 despawn = std::stoi(despawnstr.c_str());
				for (std::list<Unit*>::const_iterator iter = despawnable.begin(); iter != despawnable.end(); ++iter)
				{
					if ((*iter)->GetTypeId() != TYPEID_PLAYER)
						if ((*iter)->GetEntry() == despawn)
						{
							(*iter)->Kill((*iter), true);

						}
				}

				player->GetSession()->SendNotification("All creatures with entry '%u' ", despawn);
				player->PlayerTalkClass->SendCloseGossip();
				break;
			}

			case GOSSIP_ACTION_INFO_DEF + 14:
			{
				std::string relocatezstr;
				relocatezstr = code;
				float relocatez = std::atof(relocatezstr.c_str());
				for (std::list<Unit*>::const_iterator iter = despawnable.begin(); iter != despawnable.end(); ++iter)
				{
					if ((*iter)->GetTypeId() != TYPEID_PLAYER)
						if ((*iter)->GetEntry() == dummy)
						{
							(*iter)->GetPosition(x, y, z);
							creature->SummonCreature(dummy, x, y, z + relocatez, 0, TEMPSUMMON_CORPSE_DESPAWN);
							creature->Kill((*iter));
						}
				}
				player->GetSession()->SendNotification("Points relocated to z + '%f'", relocatez);
				player->PlayerTalkClass->SendCloseGossip();
				break;
			}


			case GOSSIP_ACTION_INFO_DEF + 11:
			{
				std::string guidstr;
				guidstr = code;
				uint32 guid = std::stoi(guidstr.c_str());
				uint32 i = 1;
				for (std::list<Unit*>::const_iterator iter = despawnable.begin(); iter != despawnable.end(); ++iter)
				{
					if ((*iter)->GetTypeId() != TYPEID_PLAYER)
						if ((*iter)->GetEntry() == dummy)
						{
							InsertingPreparedStatement(guid, i, (*iter)->GetPositionX(), (*iter)->GetPositionY(), (*iter)->GetPositionZ());
							i += 1;
						}
				}
				player->GetSession()->SendNotification("Path loaded in database '%u'", guid);
				player->PlayerTalkClass->SendCloseGossip();
				break;
			}


			case GOSSIP_ACTION_INFO_DEF + 13:
			{
				std::string datastr;
				datastr = code;
				guid = std::stoi(datastr.c_str());
				uint32 i = 1;
				for (std::list<Unit*>::const_iterator iter = despawnable.begin(); iter != despawnable.end(); ++iter)
				{
					if ((*iter)->GetTypeId() != TYPEID_PLAYER)
						if ((*iter)->GetEntry() == dummy)
						{
							(*iter)->SetUInt64Value(UNIT_FIELD_TARGET, guid);
						}
				}
				player->GetSession()->SendNotification("Data set to dummys '%u'", guid);
				player->PlayerTalkClass->SendCloseGossip();
				break;
			}

			}

		}
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{

		if (sender != GOSSIP_SENDER_MAIN)
			return false;


		std::list<Unit*> targets;
		Trinity::AnyFriendlyUnitInObjectRangeCheck u_check(creature, creature, actiondist);
		Trinity::UnitListSearcher<Trinity::AnyFriendlyUnitInObjectRangeCheck> searcher(creature, targets, u_check);
		creature->VisitNearbyObject(actiondist, searcher);
		switch (action)
		{
		case GOSSIP_ACTION_INFO_DEF + 6:
			for (std::list<Unit*>::const_iterator iter = targets.begin(); iter != targets.end(); ++iter)
			{
				if ((*iter)->GetTypeId() != TYPEID_PLAYER)
					if ((*iter)->GetEntry() == entry)
					{
						(*iter)->GetPosition(x, y, z, o);
						creature->SummonCreature(dummy, x + radius * std::cos(o + angle), y + radius * std::sin(o + angle), z, 0, TEMPSUMMON_CORPSE_DESPAWN);

					}
			}
			player->GetSession()->SendNotification("New path spawned!");
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case GOSSIP_ACTION_INFO_DEF + 7:
			entry = 0;
			dummy = 0;
			actiondist = 0;
			angle = 0;
			radius = 0;
			player->GetSession()->SendNotification("All values have been restored");
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case GOSSIP_ACTION_INFO_DEF + 9:
		{
			for (std::list<Unit*>::const_iterator iter = targets.begin(); iter != targets.end(); ++iter)
			{
				if ((*iter)->GetTypeId() != TYPEID_PLAYER)
					if ((*iter)->GetEntry() == entry)
					{
						(*iter)->SetOrientation(angle);
					}
			}
			player->GetSession()->SendNotification("Set orientation of all entrys to '%f'", angle);
			player->PlayerTalkClass->SendCloseGossip();
			break;
		}

		case GOSSIP_ACTION_INFO_DEF + 10:
			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "Input value from player", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 14, "", 0, true);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Player's current Z ", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 15);
			player->SEND_GOSSIP_MENU(1, creature->GetGUID());
			break;


		case GOSSIP_ACTION_INFO_DEF + 15:
		{

			for (std::list<Unit*>::const_iterator iter = targets.begin(); iter != targets.end(); ++iter)
			{
				if ((*iter)->GetTypeId() != TYPEID_PLAYER)
					if ((*iter)->GetEntry() == dummy)
					{
						(*iter)->GetPosition(x, y, z);
						creature->SummonCreature(dummy, x, y, player->GetPositionZ(), 0, TEMPSUMMON_CORPSE_DESPAWN);
						creature->Kill((*iter));
					}
			}
			player->GetSession()->SendNotification("Points relocated to z = '%f'", player->GetPositionZ());
			player->PlayerTalkClass->SendCloseGossip();
			break;
		}

		}
	}




	void InsertingPreparedStatement(uint32 guid, uint32 id, float position_x, float position_y, float position_z)
	{
		PreparedStatement * stmt = WorldDatabase.GetPreparedStatement(WORLD_INS_WAYPOINT_PATH);
		stmt->setUInt32(0, guid);
		stmt->setUInt32(1, id);
		stmt->setFloat(2, position_x);
		stmt->setFloat(3, position_y);
		stmt->setFloat(4, position_z);
		stmt->setUInt32(5, 0);
		stmt->setUInt32(6, 0);
		stmt->setUInt32(7, 0);
		stmt->setUInt32(8, 0);
		stmt->setUInt32(9, 100);
		stmt->setUInt32(10, 0);
		stmt->setUInt32(11, 0);
		WorldDatabase.Execute(stmt);

	}

};


void AddSC_path_creator()
{
	new path_creator();
}

/*
std::list<Unit*> targets;
Trinity::AnyFriendlyUnitInObjectRangeCheck u_check(creature, creature, distance);
Trinity::UnitListSearcher<Trinity::AnyFriendlyUnitInObjectRangeCheck> searcher(creature, targets, u_check);

bool OnGossipSelectCode(Player* player, Creature* creature, uint32 sender, uint32 action, char const* code)
{

player->PlayerTalkClass->ClearMenus();
if (sender == GOSSIP_SENDER_MAIN)
{
switch (action)
{
case GOSSIP_ACTION_INFO_DEF + 1:
{
std::string codestr;
entry = std::stoi(codestr.c_str());              // entry of the guidance creature
player->GetSession()->SendNotification("The entry you chose is '%u' ", entry);
player->PlayerTalkClass->SendCloseGossip();
break;
}

case GOSSIP_ACTION_INFO_DEF + 2:
{
std::string distancestr;
distancestr = code;
distance = std::atof(distancestr.c_str());
player->GetSession()->SendNotification("The width of the path is '%u'", distance);
player->PlayerTalkClass->SendCloseGossip();
break;
}
}
}
return true;
};
*/